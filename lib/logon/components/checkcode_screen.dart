import 'package:flutter/material.dart';
import 'package:ironman/logon/components/checkcode_provider.dart';

class CheckcodeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CheckcodeProvider(
      child: Scaffold(
          appBar: AppBar(title: Text("Подтверждение номера")),
          body: CheckcodeBody()),
    );
  }
}

class CheckcodeBody extends StatefulWidget {
  @override
  State<CheckcodeBody> createState() => _CheckcodeBodyState();
}

class _CheckcodeBodyState extends State<CheckcodeBody> {
  final shortCodeController = TextEditingController();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    final provider = CheckcodeProvider.of(context);
    provider.tokenResponse.listen((_) {
      provider.close();
      Navigator.of(context).pushReplacementNamed('/tasks');
    }, onError: (error) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
              title: Text('Ошибка'),
              content: Text(error.toString()))).then((_) {
        setState(() => this.loading = false);
        Navigator.of(context).pushReplacementNamed('/');
      });
    });
    return Container(
        padding: EdgeInsets.symmetric(vertical: 120.0, horizontal: 80.0),
        alignment: Alignment.center,
        child: loading
            ? RefreshProgressIndicator()
            : Column(children: <Widget>[
                TextField(
                    keyboardType: TextInputType.number,
                    autofocus: true,
                    controller: shortCodeController,
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        hintText: "Введите код подтвержения",
                        helperText: "См. SMS или телеграм-бота СД Рассылка"),
                    onEditingComplete: () {
                      provider.tokenRequest.add(shortCodeController.text);
                      setState(() => this.loading = true);
                    }),
                Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: FlatButton(
                        child: Text("ДАЛЕЕ"),
                        onPressed: () {
                          provider.tokenRequest.add(shortCodeController.text);
                          setState(() => this.loading = true);
                        }))
              ]));
  }

  @override
  void dispose() {
    shortCodeController.dispose();
    super.dispose();
  }
}
