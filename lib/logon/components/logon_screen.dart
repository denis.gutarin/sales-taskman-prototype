import 'package:flutter/material.dart';
import 'package:ironman/logon/components/logon_provider.dart';

class LogonScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LogonProvider(
      child: Scaffold(appBar: AppBar(title: Text("Вход")), body: LogonBody()),
    );
  }
}

class LogonBody extends StatefulWidget {
  @override
  State<LogonBody> createState() => _LogonBodyState();
}

class _LogonBodyState extends State<LogonBody> {
  final phoneController = TextEditingController();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    final provider = LogonProvider.of(context);
    provider.shortCodeResponse.listen((_) {
      Navigator.of(context).pushReplacementNamed('/checkcode');
      provider.close();
    }, onError: (error) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
              title: Text('Ошибка'),
              content: Text(error.toString()))).then((_) {
        setState(() => this.loading = false);
      });
    });
    return Container(
        padding: EdgeInsets.symmetric(vertical: 120.0, horizontal: 80.0),
        alignment: Alignment.center,
        child: loading
            ? RefreshProgressIndicator()
            : Column(children: <Widget>[
                TextField(
                    keyboardType: TextInputType.number,
                    autofocus: true,
                    controller: phoneController,
                    textAlign: TextAlign.center,
                    decoration:
                        InputDecoration(hintText: "Введите Ваш номер телефона"),
                    onEditingComplete: () {
                      provider.shortCodeRequest.add(phoneController.text);
                      setState(() => this.loading = true);
                    }),
                Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: FlatButton(
                        child: Text("ДАЛЕЕ"),
                        onPressed: () {
                          provider.shortCodeRequest.add(phoneController.text);
                          setState(() => this.loading = true);
                        }))
              ]));
  }

  @override
  void dispose() {
    phoneController.dispose();
    super.dispose();
  }
}
