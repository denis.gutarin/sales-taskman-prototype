import 'package:rxdart/rxdart.dart';
import 'package:ironman/logon/services/shortcode.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class LogonBloc {
  BehaviorSubject<String> _shortCodeRequest = BehaviorSubject<String>();
  Stream<LogonShortCodeResponse> _shortCodeStream = Stream.empty();

  Sink<String> get shortCodeRequest => _shortCodeRequest.sink;
  Stream<LogonShortCodeResponse> get shortCodeResponse => _shortCodeStream;

  LogonBloc() {
    _shortCodeStream = _shortCodeRequest.stream
        .distinct()
        .transform(phoneToRequestObject)
        .asyncMap(callLogonShortCode)
        .asBroadcastStream();
  }

  final phoneToRequestObject =
      StreamTransformer<String, LogonShortCodeRequest>.fromHandlers(
          handleData: (phone, sink) {
    if (RegExp("\\+?\\d{10,15}").hasMatch(phone)) {
      final request = LogonShortCodeRequest((it) => it..phone = phone);
      sink.add(request);
    } else {
      sink.addError('Некорректный мобильный номер');
    }
  });

  void close() {
    _shortCodeRequest.close();
  }
}

class LogonProvider extends InheritedWidget {
  final bloc;

  LogonProvider({Key key, Widget child})
      : bloc = LogonBloc(),
        super(key: key, child: child);

  static LogonBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(LogonProvider)
            as LogonProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
