import 'package:rxdart/rxdart.dart';
import 'package:ironman/logon/services/token.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class CheckcodeBloc {
  BehaviorSubject<String> _tokenRequest = BehaviorSubject<String>();
  Stream<LogonTokenResponse> _tokenResponse = Stream.empty();

  Sink<String> get tokenRequest => _tokenRequest.sink;
  Stream<LogonTokenResponse> get tokenResponse => _tokenResponse;

  CheckcodeBloc() {
    _tokenResponse = _tokenRequest.stream
        .distinct()
        .transform(phoneToRequestObject)
        .asyncMap(callLogonToken)
        .asBroadcastStream();
  }

  final phoneToRequestObject =
      StreamTransformer<String, LogonTokenRequest>.fromHandlers(
          handleData: (shortCode, sink) {
    final request = LogonTokenRequest((it) => it
      ..phone = ""
      ..short = shortCode);
    sink.add(request);
  });

  void close() {
    _tokenRequest.close();
  }
}

class CheckcodeProvider extends InheritedWidget {
  final bloc;

  CheckcodeProvider({Key key, Widget child})
      : bloc = CheckcodeBloc(),
        super(key: key, child: child);

  static CheckcodeBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(CheckcodeProvider)
            as CheckcodeProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
