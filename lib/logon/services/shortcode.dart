import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/logon/services/serializers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'shortcode.g.dart';

abstract class LogonShortCodeRequest
    implements Built<LogonShortCodeRequest, LogonShortCodeRequestBuilder> {
  LogonShortCodeRequest._();

  factory LogonShortCodeRequest([updates(LogonShortCodeRequestBuilder b)]) =
      _$LogonShortCodeRequest;

  @BuiltValueField(wireName: "PHONE")
  String get phone;

  static Serializer<LogonShortCodeRequest> get serializer =>
      _$logonShortCodeRequestSerializer;
}

abstract class LogonShortCodeResponse
    implements Built<LogonShortCodeResponse, LogonShortCodeResponseBuilder> {
  LogonShortCodeResponse._();

  factory LogonShortCodeResponse([updates(LogonShortCodeResponseBuilder b)]) =
      _$LogonShortCodeResponse;

  @BuiltValueField(wireName: "OK")
  String get ok;

  @BuiltValueField(wireName: "ERROR")
  String get error;

  static Serializer<LogonShortCodeResponse> get serializer =>
      _$logonShortCodeResponseSerializer;
}

Future<LogonShortCodeResponse> callLogonShortCode(
    LogonShortCodeRequest input) async {
  final String body = json.encode(
      serializers.serializeWith(LogonShortCodeRequest.serializer, input));
  final resp = await http.post(SHORTCODE_URL, body: body);
  final result = serializers.deserializeWith(
      LogonShortCodeResponse.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);
  final saved = await SharedPreferences.getInstance();
  saved.setString("phone", input.phone);
  return result;
}
