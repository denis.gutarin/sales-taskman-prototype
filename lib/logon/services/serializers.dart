import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:ironman/logon/services/shortcode.dart';
import 'package:ironman/logon/services/token.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  LogonShortCodeRequest,
  LogonShortCodeResponse,
  LogonTokenRequest,
  LogonTokenResponse,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
