// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shortcode.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<LogonShortCodeRequest> _$logonShortCodeRequestSerializer =
    new _$LogonShortCodeRequestSerializer();
Serializer<LogonShortCodeResponse> _$logonShortCodeResponseSerializer =
    new _$LogonShortCodeResponseSerializer();

class _$LogonShortCodeRequestSerializer
    implements StructuredSerializer<LogonShortCodeRequest> {
  @override
  final Iterable<Type> types = const [
    LogonShortCodeRequest,
    _$LogonShortCodeRequest
  ];
  @override
  final String wireName = 'LogonShortCodeRequest';

  @override
  Iterable serialize(Serializers serializers, LogonShortCodeRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'PHONE',
      serializers.serialize(object.phone,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  LogonShortCodeRequest deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LogonShortCodeRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'PHONE':
          result.phone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$LogonShortCodeResponseSerializer
    implements StructuredSerializer<LogonShortCodeResponse> {
  @override
  final Iterable<Type> types = const [
    LogonShortCodeResponse,
    _$LogonShortCodeResponse
  ];
  @override
  final String wireName = 'LogonShortCodeResponse';

  @override
  Iterable serialize(Serializers serializers, LogonShortCodeResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'OK',
      serializers.serialize(object.ok, specifiedType: const FullType(String)),
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  LogonShortCodeResponse deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LogonShortCodeResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'OK':
          result.ok = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$LogonShortCodeRequest extends LogonShortCodeRequest {
  @override
  final String phone;

  factory _$LogonShortCodeRequest(
          [void updates(LogonShortCodeRequestBuilder b)]) =>
      (new LogonShortCodeRequestBuilder()..update(updates)).build();

  _$LogonShortCodeRequest._({this.phone}) : super._() {
    if (phone == null) {
      throw new BuiltValueNullFieldError('LogonShortCodeRequest', 'phone');
    }
  }

  @override
  LogonShortCodeRequest rebuild(void updates(LogonShortCodeRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  LogonShortCodeRequestBuilder toBuilder() =>
      new LogonShortCodeRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LogonShortCodeRequest && phone == other.phone;
  }

  @override
  int get hashCode {
    return $jf($jc(0, phone.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LogonShortCodeRequest')
          ..add('phone', phone))
        .toString();
  }
}

class LogonShortCodeRequestBuilder
    implements Builder<LogonShortCodeRequest, LogonShortCodeRequestBuilder> {
  _$LogonShortCodeRequest _$v;

  String _phone;
  String get phone => _$this._phone;
  set phone(String phone) => _$this._phone = phone;

  LogonShortCodeRequestBuilder();

  LogonShortCodeRequestBuilder get _$this {
    if (_$v != null) {
      _phone = _$v.phone;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LogonShortCodeRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LogonShortCodeRequest;
  }

  @override
  void update(void updates(LogonShortCodeRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$LogonShortCodeRequest build() {
    final _$result = _$v ?? new _$LogonShortCodeRequest._(phone: phone);
    replace(_$result);
    return _$result;
  }
}

class _$LogonShortCodeResponse extends LogonShortCodeResponse {
  @override
  final String ok;
  @override
  final String error;

  factory _$LogonShortCodeResponse(
          [void updates(LogonShortCodeResponseBuilder b)]) =>
      (new LogonShortCodeResponseBuilder()..update(updates)).build();

  _$LogonShortCodeResponse._({this.ok, this.error}) : super._() {
    if (ok == null) {
      throw new BuiltValueNullFieldError('LogonShortCodeResponse', 'ok');
    }
    if (error == null) {
      throw new BuiltValueNullFieldError('LogonShortCodeResponse', 'error');
    }
  }

  @override
  LogonShortCodeResponse rebuild(
          void updates(LogonShortCodeResponseBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  LogonShortCodeResponseBuilder toBuilder() =>
      new LogonShortCodeResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LogonShortCodeResponse &&
        ok == other.ok &&
        error == other.error;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, ok.hashCode), error.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LogonShortCodeResponse')
          ..add('ok', ok)
          ..add('error', error))
        .toString();
  }
}

class LogonShortCodeResponseBuilder
    implements Builder<LogonShortCodeResponse, LogonShortCodeResponseBuilder> {
  _$LogonShortCodeResponse _$v;

  String _ok;
  String get ok => _$this._ok;
  set ok(String ok) => _$this._ok = ok;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  LogonShortCodeResponseBuilder();

  LogonShortCodeResponseBuilder get _$this {
    if (_$v != null) {
      _ok = _$v.ok;
      _error = _$v.error;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LogonShortCodeResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LogonShortCodeResponse;
  }

  @override
  void update(void updates(LogonShortCodeResponseBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$LogonShortCodeResponse build() {
    final _$result =
        _$v ?? new _$LogonShortCodeResponse._(ok: ok, error: error);
    replace(_$result);
    return _$result;
  }
}
