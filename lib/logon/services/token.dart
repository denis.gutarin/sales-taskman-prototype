import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/logon/services/serializers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'token.g.dart';

abstract class LogonTokenRequest
    implements Built<LogonTokenRequest, LogonTokenRequestBuilder> {
  LogonTokenRequest._();

  factory LogonTokenRequest([updates(LogonTokenRequestBuilder b)]) =
      _$LogonTokenRequest;

  @BuiltValueField(wireName: "PHONE")
  String get phone;

  @BuiltValueField(wireName: "SHORT")
  String get short;

  static Serializer<LogonTokenRequest> get serializer =>
      _$logonTokenRequestSerializer;
}

abstract class LogonTokenResponse
    implements Built<LogonTokenResponse, LogonTokenResponseBuilder> {
  LogonTokenResponse._();

  factory LogonTokenResponse([updates(LogonTokenResponseBuilder b)]) =
      _$LogonTokenResponse;

  @BuiltValueField(wireName: "TOKEN")
  String get token;

  @BuiltValueField(wireName: "UNAME")
  String get uname;

  @BuiltValueField(wireName: "REPEAT")
  String get repeat;

  @BuiltValueField(wireName: "ERROR")
  String get error;

  static Serializer<LogonTokenResponse> get serializer =>
      _$logonTokenResponseSerializer;
}

Future<LogonTokenResponse> callLogonToken(LogonTokenRequest input) async {
  final saved = await SharedPreferences.getInstance();
  final newInput = input.rebuild((it) => it..phone = saved.getString("phone"));
  final String body = json.encode(
      serializers.serializeWith(LogonTokenRequest.serializer, newInput));
  final resp = await http.post(TOKEN_URL, body: body);
  final result = serializers.deserializeWith(
      LogonTokenResponse.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);
  saved.setString("phone", input.phone);
  saved.setString("token", result.token);
  saved.setString("uname", result.uname);
  return result;
}
