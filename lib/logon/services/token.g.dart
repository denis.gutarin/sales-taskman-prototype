// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'token.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<LogonTokenRequest> _$logonTokenRequestSerializer =
    new _$LogonTokenRequestSerializer();
Serializer<LogonTokenResponse> _$logonTokenResponseSerializer =
    new _$LogonTokenResponseSerializer();

class _$LogonTokenRequestSerializer
    implements StructuredSerializer<LogonTokenRequest> {
  @override
  final Iterable<Type> types = const [LogonTokenRequest, _$LogonTokenRequest];
  @override
  final String wireName = 'LogonTokenRequest';

  @override
  Iterable serialize(Serializers serializers, LogonTokenRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'PHONE',
      serializers.serialize(object.phone,
          specifiedType: const FullType(String)),
      'SHORT',
      serializers.serialize(object.short,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  LogonTokenRequest deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LogonTokenRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'PHONE':
          result.phone = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SHORT':
          result.short = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$LogonTokenResponseSerializer
    implements StructuredSerializer<LogonTokenResponse> {
  @override
  final Iterable<Type> types = const [LogonTokenResponse, _$LogonTokenResponse];
  @override
  final String wireName = 'LogonTokenResponse';

  @override
  Iterable serialize(Serializers serializers, LogonTokenResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'TOKEN',
      serializers.serialize(object.token,
          specifiedType: const FullType(String)),
      'UNAME',
      serializers.serialize(object.uname,
          specifiedType: const FullType(String)),
      'REPEAT',
      serializers.serialize(object.repeat,
          specifiedType: const FullType(String)),
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  LogonTokenResponse deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new LogonTokenResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'TOKEN':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME':
          result.uname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'REPEAT':
          result.repeat = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$LogonTokenRequest extends LogonTokenRequest {
  @override
  final String phone;
  @override
  final String short;

  factory _$LogonTokenRequest([void updates(LogonTokenRequestBuilder b)]) =>
      (new LogonTokenRequestBuilder()..update(updates)).build();

  _$LogonTokenRequest._({this.phone, this.short}) : super._() {
    if (phone == null) {
      throw new BuiltValueNullFieldError('LogonTokenRequest', 'phone');
    }
    if (short == null) {
      throw new BuiltValueNullFieldError('LogonTokenRequest', 'short');
    }
  }

  @override
  LogonTokenRequest rebuild(void updates(LogonTokenRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  LogonTokenRequestBuilder toBuilder() =>
      new LogonTokenRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LogonTokenRequest &&
        phone == other.phone &&
        short == other.short;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, phone.hashCode), short.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LogonTokenRequest')
          ..add('phone', phone)
          ..add('short', short))
        .toString();
  }
}

class LogonTokenRequestBuilder
    implements Builder<LogonTokenRequest, LogonTokenRequestBuilder> {
  _$LogonTokenRequest _$v;

  String _phone;
  String get phone => _$this._phone;
  set phone(String phone) => _$this._phone = phone;

  String _short;
  String get short => _$this._short;
  set short(String short) => _$this._short = short;

  LogonTokenRequestBuilder();

  LogonTokenRequestBuilder get _$this {
    if (_$v != null) {
      _phone = _$v.phone;
      _short = _$v.short;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LogonTokenRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LogonTokenRequest;
  }

  @override
  void update(void updates(LogonTokenRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$LogonTokenRequest build() {
    final _$result =
        _$v ?? new _$LogonTokenRequest._(phone: phone, short: short);
    replace(_$result);
    return _$result;
  }
}

class _$LogonTokenResponse extends LogonTokenResponse {
  @override
  final String token;
  @override
  final String uname;
  @override
  final String repeat;
  @override
  final String error;

  factory _$LogonTokenResponse([void updates(LogonTokenResponseBuilder b)]) =>
      (new LogonTokenResponseBuilder()..update(updates)).build();

  _$LogonTokenResponse._({this.token, this.uname, this.repeat, this.error})
      : super._() {
    if (token == null) {
      throw new BuiltValueNullFieldError('LogonTokenResponse', 'token');
    }
    if (uname == null) {
      throw new BuiltValueNullFieldError('LogonTokenResponse', 'uname');
    }
    if (repeat == null) {
      throw new BuiltValueNullFieldError('LogonTokenResponse', 'repeat');
    }
    if (error == null) {
      throw new BuiltValueNullFieldError('LogonTokenResponse', 'error');
    }
  }

  @override
  LogonTokenResponse rebuild(void updates(LogonTokenResponseBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  LogonTokenResponseBuilder toBuilder() =>
      new LogonTokenResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LogonTokenResponse &&
        token == other.token &&
        uname == other.uname &&
        repeat == other.repeat &&
        error == other.error;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, token.hashCode), uname.hashCode), repeat.hashCode),
        error.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LogonTokenResponse')
          ..add('token', token)
          ..add('uname', uname)
          ..add('repeat', repeat)
          ..add('error', error))
        .toString();
  }
}

class LogonTokenResponseBuilder
    implements Builder<LogonTokenResponse, LogonTokenResponseBuilder> {
  _$LogonTokenResponse _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _uname;
  String get uname => _$this._uname;
  set uname(String uname) => _$this._uname = uname;

  String _repeat;
  String get repeat => _$this._repeat;
  set repeat(String repeat) => _$this._repeat = repeat;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  LogonTokenResponseBuilder();

  LogonTokenResponseBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _uname = _$v.uname;
      _repeat = _$v.repeat;
      _error = _$v.error;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LogonTokenResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$LogonTokenResponse;
  }

  @override
  void update(void updates(LogonTokenResponseBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$LogonTokenResponse build() {
    final _$result = _$v ??
        new _$LogonTokenResponse._(
            token: token, uname: uname, repeat: repeat, error: error);
    replace(_$result);
    return _$result;
  }
}
