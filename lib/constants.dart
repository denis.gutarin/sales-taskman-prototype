const bool dev = false;

const String SHORTCODE_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZAUTH_TOKENS_API/shortcode'
    : 'https://pas.sdvor.com/api/bot/ZAUTH_TOKENS_API/shortcode';

const String TOKEN_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZAUTH_TOKENS_API/token'
    : 'https://pas.sdvor.com/api/bot/ZAUTH_TOKENS_API/token';

const String TASKLIST_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZTODOS_DEALS_API/list'
    : 'https://pas.sdvor.com/api/bot/ZTODOS_DEALS_API/list';

const String TASKINFO_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZTODOS_DEALS_API/task'
    : 'https://pas.sdvor.com/api/bot/ZTODOS_DEALS_API/task';

const String TASKMESSAGES_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZTODOS_DEALS_API/messages'
    : 'https://pas.sdvor.com/api/bot/ZTODOS_DEALS_API/messages';

const String TASKCUSTOMERS_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZTODOS_DEALS_API/customers'
    : 'https://pas.sdvor.com/api/bot/ZTODOS_DEALS_API/customers';

const String TASKEXECUTORS_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZTODOS_DEALS_API/executors'
    : 'https://pas.sdvor.com/api/bot/ZTODOS_DEALS_API/executors';

const String TASKDATETIME_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZTODOS_DEALS_API/datetime'
    : 'https://pas.sdvor.com/api/bot/ZTODOS_DEALS_API/datetime';

const String TASKCONSTRUCTIONS_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZTODOS_DEALS_API/constructions'
    : 'https://pas.sdvor.com/api/bot/ZTODOS_DEALS_API/constructions';

const String TASKFILES_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZTODOS_DEALS_API/file'
    : 'https://pas.sdvor.com/api/bot/ZTODOS_DEALS_API/file';

const String AVATAR_URL = dev
    ? 'https://dev.pas.sdvor.com/api/bot/ZTODOS_DEALS_API/avatar/'
    : 'https://pas.sdvor.com/api/bot/ZTODOS_DEALS_API/avatar/';
