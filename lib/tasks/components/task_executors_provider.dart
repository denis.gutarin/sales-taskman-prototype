import 'package:rxdart/rxdart.dart';
import 'package:ironman/tasks/services/task_executors.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class TaskExecutorsAction {
  final String action;
  final String id;
  final String executor;
  final String search;
  TaskExecutorsAction(
      {this.action: "", this.id: "", this.executor: "", this.search: ""});
}

class TaskExecutorsBloc {
  BehaviorSubject<TaskExecutorsAction> _taskExecutorsRequest =
      BehaviorSubject<TaskExecutorsAction>();
  Stream<TaskExecutorsResponse> _taskExecutorsStream = Stream.empty();

  Sink<TaskExecutorsAction> get taskExecutorsRequest =>
      _taskExecutorsRequest.sink;
  Stream<TaskExecutorsResponse> get taskExecutorsStream => _taskExecutorsStream;

  String text = "";

  TaskExecutorsBloc(Function onNavigate(TaskExecutorsResponse response)) {
    _taskExecutorsStream = _taskExecutorsRequest.stream
        .distinct()
        .transform(toRequestObject)
        .asyncMap(callTaskExecutors)
        .asBroadcastStream();
  }

  final toRequestObject =
      StreamTransformer<TaskExecutorsAction, TaskExecutorsRequest>.fromHandlers(
          handleData: (input, sink) {
    final request = TaskExecutorsRequest((it) => it
      ..uname = ''
      ..token = ''
      ..search = input.search
      ..executor = input.executor
      ..action = input.action
      ..id = input.id);
    sink.add(request);
  });

  void close() {
    _taskExecutorsRequest.close();
  }
}

class TaskExecutorsProvider extends InheritedWidget {
  final bloc;

  TaskExecutorsProvider(
      {Key key,
      Widget child,
      Function onNavigate(TaskExecutorsResponse response)})
      : bloc = TaskExecutorsBloc(onNavigate),
        super(key: key, child: child);

  static TaskExecutorsBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(TaskExecutorsProvider)
            as TaskExecutorsProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
