import 'package:flutter/material.dart';
import 'package:ironman/tasks/components/task_list_provider.dart';
import 'package:ironman/tasks/components/task_list_view.dart';
import 'package:ironman/tasks/components/task_info_provider.dart';
import 'package:ironman/tasks/components/task_info_screen.dart';

class TaskListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TaskListProvider(child: new TaskListScreenEx());
  }
}

class TaskListScreenEx extends StatelessWidget {
  const TaskListScreenEx({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = TaskListProvider.of(context);
    return TaskInfoProvider(
        onNavigate: (response) {
          if (response == null) return;
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      TaskInfoScreen(id: response.task.id))).then((_) {
            provider.taskListRequest
                .add(TaskListAction(filter: provider.filter));
          });
        },
        child: Scaffold(
            appBar: TaskListAppBar(),
            body: TaskListView(),
            floatingActionButton: ButtonNewTask()));
  }
}

class ButtonNewTask extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = TaskInfoProvider.of(context);
    return StreamBuilder(
        stream: provider.taskInfoStream,
        builder: (context, snapshot) {
          return FloatingActionButton(
            onPressed: () {
              provider.taskInfoRequest.add(TaskInfoAction(action: 'LOADING'));
              provider.taskInfoRequest.add(TaskInfoAction(action: 'NEW'));
            },
            child: Icon(Icons.add),
          );
        });
  }
}

class TaskListAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(96.0);

  @override
  Widget build(BuildContext context) {
    final provider = TaskListProvider.of(context);
    return AppBar(
        title: Text('Айронмен'),
        bottom: PreferredSize(
            preferredSize: Size.infinite,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                    child: Text("СЕЙЧАС"),
                    onPressed: () {
                      provider.taskListRequest
                          .add(TaskListAction(action: 'LOADING'));
                      provider.filter = 'TODAY';
                      provider.taskListRequest
                          .add(TaskListAction(filter: provider.filter));
                    }),
                FlatButton(
                    child: Text("ЗАВТРА"),
                    onPressed: () {
                      provider.taskListRequest
                          .add(TaskListAction(action: 'LOADING'));
                      provider.filter = 'TOMORROW';
                      provider.taskListRequest
                          .add(TaskListAction(filter: provider.filter));
                    }),
                FlatButton(
                    child: Text("ПЛАН"),
                    onPressed: () {
                      provider.taskListRequest
                          .add(TaskListAction(action: 'LOADING'));
                      provider.filter = 'PLAN';
                      provider.taskListRequest
                          .add(TaskListAction(filter: provider.filter));
                    }),
              ],
            )));
  }
}
