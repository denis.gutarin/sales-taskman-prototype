import 'package:rxdart/rxdart.dart';
import 'package:ironman/tasks/services/task_customers.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class TaskCustomersAction {
  final String action;
  final String id;
  final String customer;
  final String search;
  TaskCustomersAction(
      {this.action: "", this.id: "", this.customer: "", this.search: ""});
}

class TaskCustomersBloc {
  final _taskCustomersRequest = BehaviorSubject<TaskCustomersAction>();
  Stream<TaskCustomersResponse> _taskCustomersStream = Stream.empty();

  Sink<TaskCustomersAction> get taskCustomersRequest =>
      _taskCustomersRequest.sink;
  Stream<TaskCustomersResponse> get taskCustomersStream => _taskCustomersStream;

  TaskCustomersBloc(Function onNavigate(TaskCustomersResponse response)) {
    _taskCustomersStream = _taskCustomersRequest.stream
        .distinct()
        .transform(toRequestObject)
        .asyncMap(callTaskCustomers)
        .asBroadcastStream();
  }

  String text = "";

  final toRequestObject =
      StreamTransformer<TaskCustomersAction, TaskCustomersRequest>.fromHandlers(
          handleData: (input, sink) {
    final request = TaskCustomersRequest((it) => it
      ..uname = ''
      ..token = ''
      ..search = input.search
      ..customer = input.customer
      ..action = input.action
      ..id = input.id);
    sink.add(request);
  });

  void close() {
    _taskCustomersRequest.close();
  }
}

class TaskCustomersProvider extends InheritedWidget {
  final bloc;

  TaskCustomersProvider(
      {Key key,
      Widget child,
      Function onNavigate(TaskCustomersResponse response)})
      : bloc = TaskCustomersBloc(onNavigate),
        super(key: key, child: child);

  static TaskCustomersBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(TaskCustomersProvider)
            as TaskCustomersProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
