import 'package:rxdart/rxdart.dart';
import 'package:ironman/tasks/services/task_constructions.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class TaskConstructionsAction {
  final String action;
  final String id;
  final String construction;
  final String search;
  TaskConstructionsAction(
      {this.action: "", this.id: "", this.construction: "", this.search: ""});
}

class TaskConstructionsBloc {
  BehaviorSubject<TaskConstructionsAction> _taskConstructionsRequest =
      BehaviorSubject<TaskConstructionsAction>();
  Stream<TaskConstructionsResponse> _taskConstructionsStream = Stream.empty();

  Sink<TaskConstructionsAction> get taskConstructionsRequest =>
      _taskConstructionsRequest.sink;
  Stream<TaskConstructionsResponse> get taskConstructionsStream =>
      _taskConstructionsStream;

  String text = "";

  TaskConstructionsBloc(
      Function onNavigate(TaskConstructionsResponse response)) {
    _taskConstructionsStream = _taskConstructionsRequest.stream
        .distinct()
        .transform(toRequestObject)
        .asyncMap(callTaskConstructions)
        .asBroadcastStream();
  }

  final toRequestObject = StreamTransformer<TaskConstructionsAction,
      TaskConstructionsRequest>.fromHandlers(handleData: (input, sink) {
    final request = TaskConstructionsRequest((it) => it
      ..uname = ''
      ..token = ''
      ..search = input.search
      ..construction = input.construction
      ..action = input.action
      ..id = input.id);
    sink.add(request);
  });

  void close() {
    _taskConstructionsRequest.close();
  }
}

class TaskConstructionsProvider extends InheritedWidget {
  final bloc;

  TaskConstructionsProvider(
      {Key key,
      Widget child,
      Function onNavigate(TaskConstructionsResponse response)})
      : bloc = TaskConstructionsBloc(onNavigate),
        super(key: key, child: child);

  static TaskConstructionsBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(TaskConstructionsProvider)
            as TaskConstructionsProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
