import 'package:rxdart/rxdart.dart';
import 'package:ironman/tasks/services/task_messages.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class TaskMessagesAction {
  final String action;
  final String id;
  final String message;
  final String fileName;
  TaskMessagesAction(
      {this.action: "", this.id: "", this.message: "", this.fileName: ""});
}

class TaskMessagesBloc {
  BehaviorSubject<TaskMessagesAction> _taskMessagesRequest =
      BehaviorSubject<TaskMessagesAction>();
  BehaviorSubject<TaskMessagesAction> _taskMessagesLoading =
      BehaviorSubject<TaskMessagesAction>();
  Stream<TaskMessagesResponse> _taskMessagesStream = Stream.empty();

  Sink<TaskMessagesAction> get taskMessagesRequest => _taskMessagesRequest.sink;
  Sink<TaskMessagesAction> get taskMessagesLoading => _taskMessagesLoading.sink;
  Stream<TaskMessagesResponse> get taskMessagesStream => _taskMessagesStream;

  TaskMessagesBloc(Function onNavigate(TaskMessagesResponse response)) {
    _taskMessagesStream = _taskMessagesRequest.stream
        .distinct()
        .transform(toRequestObject)
        .asyncMap(callTaskMessages)
        .asBroadcastStream();
  }

  final toRequestObject =
      StreamTransformer<TaskMessagesAction, TaskMessagesRequest>.fromHandlers(
          handleData: (input, sink) {
    final request = TaskMessagesRequest((it) => it
      ..uname = ''
      ..token = ''
      ..message = input.message
      ..action = input.action
      ..fileName = input.fileName
      ..id = input.id);
    sink.add(request);
  });

  void close() {
    _taskMessagesRequest.close();
  }
}

class TaskMessagesProvider extends InheritedWidget {
  final bloc;

  TaskMessagesProvider(
      {Key key,
      Widget child,
      Function onNavigate(TaskMessagesResponse response)})
      : bloc = TaskMessagesBloc(onNavigate),
        super(key: key, child: child);

  static TaskMessagesBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(TaskMessagesProvider)
            as TaskMessagesProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
