import 'package:rxdart/rxdart.dart';
import 'package:ironman/tasks/services/task_list.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class TaskListAction {
  final String filter;
  final String search;
  final String action;
  final String id;
  TaskListAction(
      {this.filter: "", this.search: "", this.action: "", this.id: ""});
}

class TaskListBloc {
  String filter = "TODAY";
  String search = "";

  BehaviorSubject<TaskListAction> _taskListRequest =
      BehaviorSubject<TaskListAction>();
  Stream<TaskList> _taskListStream = Stream.empty();

  Sink<TaskListAction> get taskListRequest => _taskListRequest.sink;
  Stream<TaskList> get taskListStream => _taskListStream;

  TaskListBloc() {
    _taskListStream = _taskListRequest.stream
        .distinct()
        .transform(toRequestObject)
        .asyncMap(callTaskList)
        .asBroadcastStream();
  }

  final toRequestObject =
      StreamTransformer<TaskListAction, TaskListRequest>.fromHandlers(
          handleData: (input, sink) {
    final request = TaskListRequest((it) => it
      ..uname = ''
      ..token = ''
      ..filter = input.filter
      ..search = input.search
      ..action = input.action
      ..id = input.id);
    sink.add(request);
  });

  void close() {
    _taskListRequest.close();
  }
}

class TaskListProvider extends InheritedWidget {
  final bloc;

  TaskListProvider({Key key, Widget child})
      : bloc = TaskListBloc(),
        super(key: key, child: child);

  static TaskListBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(TaskListProvider)
            as TaskListProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
