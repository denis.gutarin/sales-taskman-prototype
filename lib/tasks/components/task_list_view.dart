import 'package:flutter/material.dart';
import 'package:ironman/tasks/services/task_list.dart';
import 'package:ironman/tasks/services/task_info.dart';
import 'package:ironman/tasks/components/task_list_provider.dart';
import 'package:ironman/tasks/components/task_info_screen.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ironman/constants.dart';

class TaskListView extends StatelessWidget {
  Widget _buildItem(BuildContext context, TaskInfo current) {
    final provider = TaskListProvider.of(context);
    return Slidable(
        delegate: SlidableDrawerDelegate(),
        actionExtentRatio: 0.25,
        child: ListTile(
          title: Text(current.firstMessage, maxLines: 1),
          subtitle: Text(current.customerText, maxLines: 1),
          leading: CircleAvatar(
              backgroundImage: NetworkImage(
            AVATAR_URL + current.authorId,
            scale: 2.0,
          )),
          onTap: () {
            Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => TaskInfoScreen(id: current.id)))
                .then((_) {
              provider.taskListRequest
                  .add(TaskListAction(filter: provider.filter));
            });
          },
        ),
        secondaryActions: <Widget>[
          IconSlideAction(
              caption: 'Готово',
              color: Colors.green,
              icon: Icons.done,
              onTap: () => provider.taskListRequest.add(TaskListAction(
                  search: provider.search,
                  filter: provider.filter,
                  action: "DELETE",
                  id: current.id)))
        ]);
  }

  @override
  Widget build(BuildContext context) {
    final provider = TaskListProvider.of(context);
    return StreamBuilder(
        stream: provider.taskListStream,
        builder: (BuildContext context, AsyncSnapshot<TaskList> snapshot) {
          if (snapshot.hasError)
            return Center(child: Text('${snapshot.error}'));
          else if (snapshot.hasData) {
            final _taskList = snapshot.data;
            return Container(
                child: ListView.builder(
                    itemCount: _taskList.tasks.length,
                    itemBuilder: (context, index) =>
                        _buildItem(context, _taskList.tasks[index])));
          } else {
            provider.taskListRequest
                .add(TaskListAction(filter: provider.filter));
            return Center(child: RefreshProgressIndicator());
          }
        });
  }
}
