import 'package:flutter/material.dart';
import 'package:ironman/tasks/services/task_messages.dart';
import 'package:ironman/tasks/components/task_messages_provider.dart';
import 'package:ironman/tasks/components/task_files_provider.dart';
import 'package:ironman/tasks/components/task_info_provider.dart';
import 'package:ironman/constants.dart';
import 'package:file_picker/file_picker.dart';
import 'dart:convert';
import 'dart:io';

class TaskMessagesView extends StatefulWidget {
  @override
  State<TaskMessagesView> createState() {
    return _TaskMessagesViewState();
  }
}

class _TaskMessagesViewState extends State<TaskMessagesView> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: TaskMessagesProvider(
            child: TaskFilesProvider(
                child: Column(
      children: <Widget>[
        TaskMessagesListView(),
        Divider(height: 1.0),
        SendMessageView()
      ],
    ))));
  }
}

class TaskMessagesListView extends StatelessWidget {
  Widget _buildItem(
      BuildContext context, TaskMessage current, String currentUser) {
    final files = TaskFilesProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return ListTile(
      onTap: current.attachement.isEmpty
          ? null
          : () {
              files.taskFilesRequest.add(TaskFilesAction(
                  attachement: current.attachement, id: task.id));
            },
      subtitle: Text(current.text,
          style: TextStyle(
              color:
                  current.attachement.isEmpty ? Colors.black87 : Colors.blue)),
      title: Text(current.userText + '. ' + current.dateTimeText,
          style: currentUser == current.user
              ? TextStyle(color: Colors.red)
              : TextStyle(color: Colors.blue)),
      leading: CircleAvatar(
          backgroundImage: NetworkImage(
        AVATAR_URL + current.user,
        scale: 2.0,
      )),
    );
  }

  @override
  Widget build(BuildContext context) {
    final provider = TaskMessagesProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return StreamBuilder(
        stream: provider.taskMessagesStream,
        builder: (BuildContext context,
            AsyncSnapshot<TaskMessagesResponse> snapshot) {
          if (snapshot.hasError)
            return Center(child: Text('${snapshot.error}'));
          else if (snapshot.hasData) {
            return Flexible(
                flex: 9,
                child: ListView.builder(
                  reverse: true,
                  itemCount: snapshot.data.messages.length,
                  itemBuilder: (context, index) => _buildItem(
                      context,
                      snapshot.data.messages.reversed.toList()[index],
                      snapshot.data.currentUser),
                ));
          } else {
            provider.taskMessagesRequest.add(
                TaskMessagesAction(id: task.id, action: "GET", message: ''));
            return Center(child: RefreshProgressIndicator());
          }
        });
  }
}

class SendMessageView extends StatefulWidget {
  @override
  State<SendMessageView> createState() {
    return _SendMessageViewState();
  }
}

class _SendMessageViewState extends State<SendMessageView> {
  final inputController = TextEditingController();

  Widget buildAttachmentButton(BuildContext context) {
    final task = TaskInfoProvider.of(context);
    final messages = TaskMessagesProvider.of(context);
    return Container(
        child: PopupMenuButton<FileType>(
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Icon(Icons.attach_file,
                    color: Theme.of(context).accentColor)),
            onSelected: (value) async {
              final path = await FilePicker.getFilePath(type: value);
              final bytes = await File(path).readAsBytes();
              final fileName = path.split('/').toList().last;
              messages.taskMessagesRequest
                  .add(TaskMessagesAction(action: 'LOADING'));
              messages.taskMessagesRequest.add(TaskMessagesAction(
                  fileName: fileName,
                  message: base64Encode(bytes),
                  id: task.id,
                  action: 'SENDFILE'));
            },
            itemBuilder: (context) => <PopupMenuItem<FileType>>[
                  PopupMenuItem(value: FileType.CAPTURE, child: Text("Камера")),
                  PopupMenuItem(value: FileType.IMAGE, child: Text("Галерея")),
                  PopupMenuItem(value: FileType.PDF, child: Text("PDF")),
                ]));
  }

  Widget buildTextField(BuildContext context) {
    final task = TaskInfoProvider.of(context);
    final messages = TaskMessagesProvider.of(context);
    return Flexible(
        child: TextField(
      autofocus: true,
      maxLines: null,
      controller: inputController,
      decoration: InputDecoration.collapsed(hintText: 'Введите сообщение'),
      onEditingComplete: () {
        messages.taskMessagesRequest.add(TaskMessagesAction(action: 'LOADING'));
        messages.taskMessagesRequest.add(TaskMessagesAction(
            message: inputController.text, id: task.id, action: 'SEND'));
        inputController.clear();
      },
    ));
  }

  Widget buildSendButton(BuildContext context) {
    final task = TaskInfoProvider.of(context);
    final messages = TaskMessagesProvider.of(context);
    return Container(
        child: IconButton(
      icon: Icon(Icons.send),
      color: Theme.of(context).accentColor,
      onPressed: () {
        if (inputController.text.isEmpty) return;
        messages.taskMessagesRequest.add(TaskMessagesAction(action: 'LOADING'));
        messages.taskMessagesRequest.add(TaskMessagesAction(
            message: inputController.text, id: task.id, action: 'SEND'));
        inputController.clear();
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 4.0),
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
        ),
        child: Row(children: <Widget>[
          buildAttachmentButton(context),
          buildTextField(context),
          buildSendButton(context)
        ]));
  }

  @override
  void dispose() {
    inputController.dispose();
    super.dispose();
  }
}
