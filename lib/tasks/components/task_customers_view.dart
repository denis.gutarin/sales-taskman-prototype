import 'package:flutter/material.dart';
import 'package:ironman/tasks/services/task_customers.dart';
import 'package:ironman/tasks/components/task_customers_provider.dart';
import 'package:ironman/tasks/components/task_info_provider.dart';

class TaskCustomersView extends StatefulWidget {
  @override
  State<TaskCustomersView> createState() {
    return _TaskCustomersViewState();
  }
}

class _TaskCustomersViewState extends State<TaskCustomersView> {
  @override
  Widget build(BuildContext context) {
    return TaskCustomersProvider(
        child: Column(
      children: <Widget>[
        TaskCustomersSearchBox(),
        TaskCustomersListView(),
      ],
    ));
  }
}

class TaskCustomersSearchBox extends StatefulWidget {
  @override
  State<TaskCustomersSearchBox> createState() {
    return _TaskCustomersSearchBoxState();
  }
}

class _TaskCustomersSearchBoxState extends State<TaskCustomersSearchBox> {
  final searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final provider = TaskCustomersProvider.of(context);
    return Container(
        padding: EdgeInsets.all(16.0),
        child: TextField(
          controller: searchController,
          decoration: InputDecoration(hintText: "Поиск..."),
          onChanged: (text) {
            if (text.length < 3) text = '';
            provider.text = text;
            provider.taskCustomersRequest
                .add(TaskCustomersAction(action: 'LOADING'));
          },
        ));
  }
}

class TaskCustomersListView extends StatefulWidget {
  @override
  State<TaskCustomersListView> createState() {
    return _TaskCustomersListViewState();
  }
}

class _TaskCustomersListViewState extends State<TaskCustomersListView> {
  final _scrollController = ScrollController();

  Widget _buildItem(BuildContext context, TaskCustomer current) {
    final idText = current.id.replaceFirst(RegExp(r'0+'), '');
    final provider = TaskCustomersProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return ListTileTheme(
        selectedColor: Colors.green,
        child: ListTile(
            onTap: () {
              provider.taskCustomersRequest
                  .add(TaskCustomersAction(action: 'LOADING'));
              provider.taskCustomersRequest.add(TaskCustomersAction(
                  id: task.id,
                  action: "SET",
                  customer: current.id,
                  search: provider.text));
              _scrollController.animateTo(0.0,
                  curve: Curves.ease, duration: Duration(milliseconds: 500));
              FocusScope.of(context).requestFocus(FocusNode());
            },
            subtitle: Text("Код: $idText, ИНН: ${current.inn}"),
            title: Text(current.text, maxLines: 1),
            selected: current.selected.isNotEmpty));
  }

  @override
  Widget build(BuildContext context) {
    final provider = TaskCustomersProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return StreamBuilder(
        stream: provider.taskCustomersStream,
        builder: (BuildContext context,
            AsyncSnapshot<TaskCustomersResponse> snapshot) {
          if (snapshot.hasError)
            return Center(child: Text('${snapshot.error}'));
          else if (snapshot.hasData) {
            return Flexible(
                flex: 9,
                child: ListView.builder(
                  controller: _scrollController,
                  itemCount: snapshot.data.customers.length,
                  itemBuilder: (context, index) =>
                      _buildItem(context, snapshot.data.customers[index]),
                ));
          } else {
            provider.taskCustomersRequest.add(TaskCustomersAction(
                id: task.id,
                action: "GET",
                customer: '',
                search: provider.text));
            return Center(child: RefreshProgressIndicator());
          }
        });
  }
}
