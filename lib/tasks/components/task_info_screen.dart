import 'package:flutter/material.dart';
import 'package:ironman/tasks/components/task_info_provider.dart';
import 'package:ironman/tasks/components/task_messages_view.dart';
import 'package:ironman/tasks/components/task_customers_view.dart';
import 'package:ironman/tasks/components/task_executors_view.dart';
import 'package:ironman/tasks/components/task_datetime_view.dart';
import 'package:ironman/tasks/components/task_constructions_view.dart';

class TaskInfoScreen extends StatelessWidget {
  final String id;
  TaskInfoScreen({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TaskInfoProvider(id: id, child: TaskInfoContainer());
  }
}

class TaskInfoContainer extends StatefulWidget {
  @override
  State<TaskInfoContainer> createState() {
    return _TaskInfoContainerState();
  }
}

class _TaskInfoContainerState extends State<TaskInfoContainer>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: 5);
    tabController.addListener(() {
      FocusScope.of(context).requestFocus(FocusNode());
    });
  }

  @override
  Widget build(BuildContext context) {
    final provider = TaskInfoProvider.of(context);
    return Scaffold(
        appBar: AppBar(
            title:
                Text("Задача №" + provider.id.replaceFirst(RegExp(r'0+'), '')),
            bottom: TabBar(
              controller: tabController,
              tabs: <Widget>[
                Tab(icon: Icon(Icons.send)),
                Tab(icon: Icon(Icons.local_library)),
                Tab(icon: Icon(Icons.person)),
                Tab(icon: Icon(Icons.place)),
                Tab(
                  icon: Icon(Icons.date_range),
                ),
              ],
            )),
        body: TabBarView(
          controller: tabController,
          children: <Widget>[
            TaskMessagesView(),
            TaskCustomersView(),
            TaskExecutorsView(),
            TaskConstructionsView(),
            TaskDatetimeView(tabController),
          ],
        ));
  }
}
