import 'package:flutter/material.dart';
import 'package:ironman/tasks/components/task_datetime_provider.dart';
import 'package:ironman/tasks/components/task_info_provider.dart';
import 'package:ironman/tasks/services/task_datetime.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:intl/date_symbol_data_local.dart';

class TaskDatetimeView extends StatefulWidget {
  final TabController _tabController;
  TaskDatetimeView(this._tabController);
  @override
  State<TaskDatetimeView> createState() {
    return _TaskDatetimeViewState();
  }
}

class _TaskDatetimeViewState extends State<TaskDatetimeView> {
  @override
  void initState() {
    super.initState();
    initializeDateFormatting();
  }

  @override
  Widget build(BuildContext context) {
    return TaskDatetimeProvider(
        child: Dismissible(
            key: ValueKey(1),
            onDismissed: (direction) {
              if (direction == DismissDirection.endToStart) {
                Navigator.pop(context, 'Refresh');
              } else {
                widget._tabController.animateTo(3,
                    curve: Curves.ease, duration: Duration(milliseconds: 100));
              }
            },
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 48.0),
                child: Row(
                  children: <Widget>[
                    TaskDateListView(),
                    TaskTimeListView(),
                  ],
                ))));
  }
}

class FormatedDate {
  final DateTime datetime;
  final String title;
  final String subtitle;
  FormatedDate({this.datetime, this.title, this.subtitle});
}

class TaskDateListView extends StatelessWidget {
  Widget _buildItem(
      BuildContext context, FormatedDate current, DateTime selectedDate) {
    final provider = TaskDatetimeProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return ListTileTheme(
        selectedColor: Colors.green,
        textColor:
            current.subtitle == 'суббота' || current.subtitle == 'воскресенье'
                ? Colors.grey
                : Colors.black,
        child: ListTile(
          onTap: () {
            final dt = current.datetime;
            provider.taskDatetimeRequest
                .add(TaskDatetimeAction(action: 'LOADING'));
            provider.taskDatetimeRequest.add(
              TaskDatetimeAction(
                  id: task.id,
                  action: "SET",
                  date: "${dt.year}-${dt.month}-${dt.day}"),
            );
          },
          selected: current.datetime.day == selectedDate.day &&
                  current.datetime.month == selectedDate.month &&
                  current.datetime.year == selectedDate.year
              ? true
              : false,
          title: Center(child: Text(current.title)),
          subtitle: Center(child: Text(current.subtitle)),
        ));
  }

  @override
  Widget build(BuildContext context) {
    final provider = TaskDatetimeProvider.of(context);
    final task = TaskInfoProvider.of(context);
    final dates = List<FormatedDate>.generate(92, (i) {
      var date = DateTime.now();
      date = date.add(Duration(days: i));
      return FormatedDate(
          datetime: date,
          title: DateFormat.MMMMd('ru').format(date),
          subtitle: DateFormat.EEEE('ru').format(date));
    });
    return StreamBuilder(
        stream: provider.taskDatetimeStream,
        builder: (BuildContext context,
            AsyncSnapshot<TaskDatetimeResponse> snapshot) {
          if (snapshot.hasError)
            return Center(child: Text('${snapshot.error}'));
          else if (snapshot.hasData) {
            return Flexible(
                child: ListView.builder(
              itemCount: dates.length,
              itemBuilder: (context, index) => _buildItem(
                  context, dates[index], DateTime.parse(snapshot.data.date)),
            ));
          } else {
            provider.taskDatetimeRequest
                .add(TaskDatetimeAction(id: task.id, action: "GET"));
            return Center(child: RefreshProgressIndicator());
          }
        });
  }
}

class TaskTimeListView extends StatelessWidget {
  Widget _buildItem(
      BuildContext context, FormatedDate current, DateTime selectedDate) {
    final provider = TaskDatetimeProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return ListTileTheme(
        selectedColor: Colors.green,
        child: ListTile(
          onTap: () {
            final dt = current.datetime;
            provider.taskDatetimeRequest
                .add(TaskDatetimeAction(action: 'LOADING'));
            provider.taskDatetimeRequest.add(
              TaskDatetimeAction(
                  id: task.id,
                  action: "SET",
                  time: DateFormat.Hms('ru').format(dt)),
            );
          },
          selected: current.datetime.hour == selectedDate.hour &&
                  current.datetime.minute == selectedDate.minute
              ? true
              : false,
          title: Center(child: Text(current.title)),
        ));
  }

  @override
  Widget build(BuildContext context) {
    final provider = TaskDatetimeProvider.of(context);
    final task = TaskInfoProvider.of(context);
    final dates = List<FormatedDate>.generate(23, (i) {
      var date = DateTime(2000, 1, 1, 8, 0);
      date = date.add(Duration(minutes: 30 * i));
      return FormatedDate(
          datetime: date, title: DateFormat.Hm('ru').format(date));
    });
    return StreamBuilder(
        stream: provider.taskDatetimeStream,
        builder: (BuildContext context,
            AsyncSnapshot<TaskDatetimeResponse> snapshot) {
          if (snapshot.hasError)
            return Center(child: Text('${snapshot.error}'));
          else if (snapshot.hasData) {
            return Flexible(
                child: ListView.builder(
              itemCount: dates.length,
              itemBuilder: (context, index) => _buildItem(
                  context,
                  dates[index],
                  DateTime.parse('2000-01-01T' +
                      snapshot.data.time.replaceFirst(RegExp(r'\s'), '0'))),
            ));
          } else {
            provider.taskDatetimeRequest
                .add(TaskDatetimeAction(id: task.id, action: "GET"));
            return Center(child: RefreshProgressIndicator());
          }
        });
  }
}
