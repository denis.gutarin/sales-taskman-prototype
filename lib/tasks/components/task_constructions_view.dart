import 'package:flutter/material.dart';
import 'package:ironman/tasks/services/task_constructions.dart';
import 'package:ironman/tasks/components/task_constructions_provider.dart';
import 'package:ironman/tasks/components/task_info_provider.dart';

class TaskConstructionsView extends StatefulWidget {
  @override
  State<TaskConstructionsView> createState() {
    return _TaskConstructionsViewState();
  }
}

class _TaskConstructionsViewState extends State<TaskConstructionsView> {
  @override
  Widget build(BuildContext context) {
    return TaskConstructionsProvider(
        child: Column(
      children: <Widget>[
        TaskConstructionsSearchBox(),
        TaskConstructionsListView(),
      ],
    ));
  }
}

class TaskConstructionsSearchBox extends StatefulWidget {
  @override
  State<TaskConstructionsSearchBox> createState() {
    return _TaskConstructionsSearchBoxState();
  }
}

class _TaskConstructionsSearchBoxState
    extends State<TaskConstructionsSearchBox> {
  final searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final provider = TaskConstructionsProvider.of(context);
    return Container(
        padding: EdgeInsets.all(16.0),
        child: TextField(
          controller: searchController,
          decoration: InputDecoration(hintText: "Поиск..."),
          onChanged: (text) {
            if (text.length < 3) text = '';
            provider.text = text;
            provider.taskConstructionsRequest
                .add(TaskConstructionsAction(action: 'LOADING'));
          },
        ));
  }
}

class TaskConstructionsListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TaskConstructionsListViewState();
  }
}

class _TaskConstructionsListViewState extends State<TaskConstructionsListView> {
  final _scrollController = ScrollController();

  Widget _buildItem(BuildContext context, TaskConstruction current) {
    final provider = TaskConstructionsProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return ListTileTheme(
        selectedColor: Colors.green,
        child: ListTile(
            onTap: () {
              provider.taskConstructionsRequest
                  .add(TaskConstructionsAction(action: 'LOADING'));
              provider.taskConstructionsRequest.add(TaskConstructionsAction(
                  id: task.id,
                  action: "SET",
                  construction: current.id,
                  search: provider.text));
              _scrollController.animateTo(0.0,
                  curve: Curves.ease, duration: Duration(milliseconds: 500));
              FocusScope.of(context).requestFocus(FocusNode());
            },
            selected: current.selected.isNotEmpty,
            title: Text(current.text, maxLines: 2)));
  }

  @override
  Widget build(BuildContext context) {
    final provider = TaskConstructionsProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return StreamBuilder(
        stream: provider.taskConstructionsStream,
        builder: (BuildContext context,
            AsyncSnapshot<TaskConstructionsResponse> snapshot) {
          if (snapshot.hasError)
            return Center(child: Text('${snapshot.error}'));
          else if (snapshot.hasData) {
            return Flexible(
                flex: 9,
                child: ListView.builder(
                  controller: _scrollController,
                  itemCount: snapshot.data.constructions.length,
                  itemBuilder: (context, index) =>
                      _buildItem(context, snapshot.data.constructions[index]),
                ));
          } else {
            provider.taskConstructionsRequest.add(TaskConstructionsAction(
                id: task.id,
                action: "GET",
                construction: '',
                search: provider.text));

            return Center(child: RefreshProgressIndicator());
          }
        });
  }
}
