import 'package:rxdart/rxdart.dart';
import 'package:ironman/tasks/services/task_datetime.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class TaskDatetimeAction {
  final String action;
  final String id;
  final String date;
  final String time;
  TaskDatetimeAction(
      {this.action: "", this.id: "", this.date: "", this.time: ""});
}

class TaskDatetimeBloc {
  BehaviorSubject<TaskDatetimeAction> _taskDatetimeRequest =
      BehaviorSubject<TaskDatetimeAction>();
  Stream<TaskDatetimeResponse> _taskDatetimeStream = Stream.empty();

  Sink<TaskDatetimeAction> get taskDatetimeRequest => _taskDatetimeRequest.sink;
  Stream<TaskDatetimeResponse> get taskDatetimeStream => _taskDatetimeStream;

  TaskDatetimeBloc(Function onNavigate(TaskDatetimeResponse response)) {
    _taskDatetimeStream = _taskDatetimeRequest.stream
        .distinct()
        .transform(toRequestObject)
        .asyncMap(callTaskDatetime)
        .asBroadcastStream();
  }

  final toRequestObject =
      StreamTransformer<TaskDatetimeAction, TaskDatetimeRequest>.fromHandlers(
          handleData: (input, sink) {
    final request = TaskDatetimeRequest((it) => it
      ..uname = ''
      ..token = ''
      ..time = input.time
      ..date = input.date
      ..action = input.action
      ..id = input.id);
    sink.add(request);
  });

  void close() {
    _taskDatetimeRequest.close();
  }
}

class TaskDatetimeProvider extends InheritedWidget {
  final bloc;

  TaskDatetimeProvider(
      {Key key,
      Widget child,
      Function onNavigate(TaskDatetimeResponse response)})
      : bloc = TaskDatetimeBloc(onNavigate),
        super(key: key, child: child);

  static TaskDatetimeBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(TaskDatetimeProvider)
            as TaskDatetimeProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
