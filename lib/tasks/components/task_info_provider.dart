import 'package:rxdart/rxdart.dart';
import 'package:ironman/tasks/services/task_info.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class TaskInfoAction {
  final String action;
  final String id;
  TaskInfoAction({this.action: "", this.id: ""});
}

class TaskInfoBloc {
  BehaviorSubject<TaskInfoAction> _taskInfoRequest =
      BehaviorSubject<TaskInfoAction>();
  Stream<TaskInfoResponse> _taskInfoStream = Stream.empty();

  Sink<TaskInfoAction> get taskInfoRequest => _taskInfoRequest.sink;
  Stream<TaskInfoResponse> get taskInfoStream => _taskInfoStream;
  final String id;

  TaskInfoBloc(this.id, Function onNavigate(TaskInfoResponse response)) {
    _taskInfoStream = _taskInfoRequest.stream
        .distinct()
        .transform(toRequestObject)
        .asyncMap(callTaskInfo)
        .asBroadcastStream();
    _taskInfoStream.listen((response) {
      onNavigate(response);
    });
  }

  final toRequestObject =
      StreamTransformer<TaskInfoAction, TaskInfoRequest>.fromHandlers(
          handleData: (input, sink) {
    final request = TaskInfoRequest((it) => it
      ..uname = ''
      ..token = ''
      ..action = input.action
      ..id = input.id);
    sink.add(request);
  });

  void close() {
    _taskInfoRequest.close();
  }
}

class TaskInfoProvider extends InheritedWidget {
  final bloc;

  TaskInfoProvider(
      {Key key,
      String id,
      Widget child,
      Function onNavigate(TaskInfoResponse response)})
      : bloc = TaskInfoBloc(id, onNavigate),
        super(key: key, child: child);

  static TaskInfoBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(TaskInfoProvider)
            as TaskInfoProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
