import 'package:rxdart/rxdart.dart';
import 'package:ironman/tasks/services/task_files.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

class TaskFilesAction {
  final String id;
  final String attachement;
  TaskFilesAction({this.id: "", this.attachement: ""});
}

class TaskFilesBloc {
  BehaviorSubject<TaskFilesAction> _taskFilesRequest =
      BehaviorSubject<TaskFilesAction>();
  Stream<TaskFilesResponse> _taskFilesStream = Stream.empty();

  Sink<TaskFilesAction> get taskFilesRequest => _taskFilesRequest.sink;
  Stream<TaskFilesResponse> get taskFilesStream => _taskFilesStream;

  TaskFilesBloc(Function onNavigate(TaskFilesResponse response)) {
    _taskFilesStream = _taskFilesRequest.stream
        .distinct()
        .transform(toRequestObject)
        .asyncMap(callTaskFiles)
        .asBroadcastStream();
    _taskFilesStream.listen((data) async {
      if (data.error.isNotEmpty) return;
      Directory dir = await getApplicationDocumentsDirectory();
      final binary = base64Decode(data.data);
      File(dir.path + '/' + data.fileName).writeAsBytes(binary).then((value) {
        OpenFile.open(value.path);
      });
    });
  }

  final toRequestObject =
      StreamTransformer<TaskFilesAction, TaskFilesRequest>.fromHandlers(
          handleData: (input, sink) {
    final request = TaskFilesRequest((it) => it
      ..uname = ''
      ..token = ''
      ..attachement = input.attachement
      ..id = input.id);
    sink.add(request);
  });

  void close() {
    _taskFilesRequest.close();
  }
}

class TaskFilesProvider extends InheritedWidget {
  final bloc;

  TaskFilesProvider(
      {Key key, Widget child, Function onNavigate(TaskFilesResponse response)})
      : bloc = TaskFilesBloc(onNavigate),
        super(key: key, child: child);

  static TaskFilesBloc of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(TaskFilesProvider)
            as TaskFilesProvider)
        .bloc;
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}
