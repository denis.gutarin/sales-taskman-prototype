import 'package:flutter/material.dart';
import 'package:ironman/tasks/services/task_executors.dart';
import 'package:ironman/tasks/components/task_executors_provider.dart';
import 'package:ironman/tasks/components/task_info_provider.dart';
import 'package:ironman/constants.dart';

class TaskExecutorsView extends StatefulWidget {
  @override
  State<TaskExecutorsView> createState() {
    return _TaskExecutorsViewState();
  }
}

class _TaskExecutorsViewState extends State<TaskExecutorsView> {
  @override
  Widget build(BuildContext context) {
    return TaskExecutorsProvider(
        child: Column(
      children: <Widget>[
        TaskExecutorsSearchBox(),
        TaskExecutorsListView(),
      ],
    ));
  }
}

class TaskExecutorsSearchBox extends StatefulWidget {
  @override
  State<TaskExecutorsSearchBox> createState() {
    return _TaskExecutorsSearchBoxState();
  }
}

class _TaskExecutorsSearchBoxState extends State<TaskExecutorsSearchBox> {
  final searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final provider = TaskExecutorsProvider.of(context);

    return Container(
        padding: EdgeInsets.all(16.0),
        child: TextField(
          controller: searchController,
          decoration: InputDecoration(hintText: "Поиск..."),
          onChanged: (text) {
            if (text.length < 3) text = '';
            provider.text = text;
            provider.taskExecutorsRequest
                .add(TaskExecutorsAction(action: 'LOADING'));
          },
        ));
  }
}

class TaskExecutorsListView extends StatefulWidget {
  @override
  State<TaskExecutorsListView> createState() {
    return _TaskExecutorsListViewState();
  }
}

class _TaskExecutorsListViewState extends State<TaskExecutorsListView> {
  final _scrollController = ScrollController();

  Widget _buildItem(BuildContext context, TaskExecutor current) {
    final provider = TaskExecutorsProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return ListTileTheme(
        selectedColor: Colors.green,
        child: ListTile(
            onTap: () {
              provider.taskExecutorsRequest
                  .add(TaskExecutorsAction(action: 'LOADING'));
              provider.taskExecutorsRequest.add(TaskExecutorsAction(
                  id: task.id,
                  action: "SET",
                  executor: current.id,
                  search: provider.text));

              _scrollController.animateTo(0.0,
                  curve: Curves.ease, duration: Duration(milliseconds: 500));
              FocusScope.of(context).requestFocus(FocusNode());
            },
            subtitle: Text("Код: ${current.id}"),
            title: Text(current.text),
            selected: current.selected.isNotEmpty,
            leading: CircleAvatar(
                backgroundImage: NetworkImage(
              AVATAR_URL + current.id,
              scale: 2.0,
            ))));
  }

  @override
  Widget build(BuildContext context) {
    final provider = TaskExecutorsProvider.of(context);
    final task = TaskInfoProvider.of(context);
    return StreamBuilder(
        stream: provider.taskExecutorsStream,
        builder: (BuildContext context,
            AsyncSnapshot<TaskExecutorsResponse> snapshot) {
          if (snapshot.hasError)
            return Center(child: Text('${snapshot.error}'));
          else if (snapshot.hasData) {
            return Flexible(
                flex: 9,
                child: ListView.builder(
                  controller: _scrollController,
                  itemCount: snapshot.data.executors.length,
                  itemBuilder: (context, index) =>
                      _buildItem(context, snapshot.data.executors[index]),
                ));
          } else {
            provider.taskExecutorsRequest.add(TaskExecutorsAction(
                id: task.id,
                action: "GET",
                executor: '',
                search: provider.text));

            return Center(child: RefreshProgressIndicator());
          }
        });
  }
}
