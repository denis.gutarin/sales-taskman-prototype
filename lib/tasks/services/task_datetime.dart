import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/tasks/services/serializers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'task_datetime.g.dart';

abstract class TaskDatetimeRequest
    implements Built<TaskDatetimeRequest, TaskDatetimeRequestBuilder> {
  TaskDatetimeRequest._();
  factory TaskDatetimeRequest([updates(TaskDatetimeRequestBuilder b)]) =
      _$TaskDatetimeRequest;

  @BuiltValueField(wireName: "TOKEN")
  String get token;

  @BuiltValueField(wireName: "UNAME")
  String get uname;

  @BuiltValueField(wireName: "ACTION")
  String get action;

  @BuiltValueField(wireName: "ID")
  String get id;

  @BuiltValueField(wireName: 'DATE')
  String get date;

  @BuiltValueField(wireName: 'TIME')
  String get time;

  static Serializer<TaskDatetimeRequest> get serializer =>
      _$taskDatetimeRequestSerializer;
}

abstract class TaskDatetimeResponse
    implements Built<TaskDatetimeResponse, TaskDatetimeResponseBuilder> {
  TaskDatetimeResponse._();

  @BuiltValueField(wireName: 'ERROR')
  String get error;

  @BuiltValueField(wireName: 'UNAUTHORIZED')
  String get unauthorized;

  @BuiltValueField(wireName: 'DATE')
  String get date;

  @BuiltValueField(wireName: 'TIME')
  String get time;

  factory TaskDatetimeResponse([updates(TaskDatetimeResponseBuilder b)]) =
      _$TaskDatetimeResponse;

  static Serializer<TaskDatetimeResponse> get serializer =>
      _$taskDatetimeResponseSerializer;
}

Future<TaskDatetimeResponse> callTaskDatetime(TaskDatetimeRequest input) async {
  if (input.action == 'LOADING') return null;
  final saved = await SharedPreferences.getInstance();
  final newInput = input.rebuild((it) => it
    ..uname = saved.getString("uname")
    ..token = saved.getString("token"));
  final String body = json.encode(
      serializers.serializeWith(TaskDatetimeRequest.serializer, newInput));
  final resp = await http.post(TASKDATETIME_URL, body: body);
  final result = serializers.deserializeWith(
      TaskDatetimeResponse.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);

  return result;
}
