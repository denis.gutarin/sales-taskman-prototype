import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/tasks/services/serializers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'task_files.g.dart';

abstract class TaskFilesRequest
    implements Built<TaskFilesRequest, TaskFilesRequestBuilder> {
  TaskFilesRequest._();
  factory TaskFilesRequest([updates(TaskFilesRequestBuilder b)]) =
      _$TaskFilesRequest;

  @BuiltValueField(wireName: "TOKEN")
  String get token;

  @BuiltValueField(wireName: "UNAME")
  String get uname;

  @BuiltValueField(wireName: "ID")
  String get id;

  @BuiltValueField(wireName: 'ATTACHEMENT')
  String get attachement;

  static Serializer<TaskFilesRequest> get serializer =>
      _$taskFilesRequestSerializer;
}

abstract class TaskFilesResponse
    implements Built<TaskFilesResponse, TaskFilesResponseBuilder> {
  TaskFilesResponse._();

  @BuiltValueField(wireName: 'ERROR')
  String get error;

  @BuiltValueField(wireName: 'UNAUTHORIZED')
  String get unauthorized;

  @BuiltValueField(wireName: 'FILENAME')
  String get fileName;

  @BuiltValueField(wireName: 'DATA')
  String get data;

  factory TaskFilesResponse([updates(TaskFilesResponseBuilder b)]) =
      _$TaskFilesResponse;

  static Serializer<TaskFilesResponse> get serializer =>
      _$taskFilesResponseSerializer;
}

Future<TaskFilesResponse> callTaskFiles(TaskFilesRequest input) async {
  if (input.attachement == 'LOADING') return null;
  final saved = await SharedPreferences.getInstance();
  final newInput = input.rebuild((it) => it
    ..uname = saved.getString("uname")
    ..token = saved.getString("token"));
  final String body = json
      .encode(serializers.serializeWith(TaskFilesRequest.serializer, newInput));
  final resp = await http.post(TASKFILES_URL, body: body);
  final result = serializers.deserializeWith(
      TaskFilesResponse.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);

  return result;
}
