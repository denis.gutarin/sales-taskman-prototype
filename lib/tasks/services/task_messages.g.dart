// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_messages.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<TaskMessage> _$taskMessageSerializer = new _$TaskMessageSerializer();
Serializer<TaskMessagesRequest> _$taskMessagesRequestSerializer =
    new _$TaskMessagesRequestSerializer();
Serializer<TaskMessagesResponse> _$taskMessagesResponseSerializer =
    new _$TaskMessagesResponseSerializer();

class _$TaskMessageSerializer implements StructuredSerializer<TaskMessage> {
  @override
  final Iterable<Type> types = const [TaskMessage, _$TaskMessage];
  @override
  final String wireName = 'TaskMessage';

  @override
  Iterable serialize(Serializers serializers, TaskMessage object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'DATETIME_TEXT',
      serializers.serialize(object.dateTimeText,
          specifiedType: const FullType(String)),
      'USER',
      serializers.serialize(object.user, specifiedType: const FullType(String)),
      'USER_TEXT',
      serializers.serialize(object.userText,
          specifiedType: const FullType(String)),
      'TEXT',
      serializers.serialize(object.text, specifiedType: const FullType(String)),
      'ATTACHEMENT',
      serializers.serialize(object.attachement,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskMessage deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskMessageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'DATETIME_TEXT':
          result.dateTimeText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'USER':
          result.user = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'USER_TEXT':
          result.userText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'TEXT':
          result.text = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ATTACHEMENT':
          result.attachement = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskMessagesRequestSerializer
    implements StructuredSerializer<TaskMessagesRequest> {
  @override
  final Iterable<Type> types = const [
    TaskMessagesRequest,
    _$TaskMessagesRequest
  ];
  @override
  final String wireName = 'TaskMessagesRequest';

  @override
  Iterable serialize(Serializers serializers, TaskMessagesRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'TOKEN',
      serializers.serialize(object.token,
          specifiedType: const FullType(String)),
      'UNAME',
      serializers.serialize(object.uname,
          specifiedType: const FullType(String)),
      'ACTION',
      serializers.serialize(object.action,
          specifiedType: const FullType(String)),
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'FILENAME',
      serializers.serialize(object.fileName,
          specifiedType: const FullType(String)),
      'MESSAGE',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskMessagesRequest deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskMessagesRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'TOKEN':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME':
          result.uname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ACTION':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'FILENAME':
          result.fileName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'MESSAGE':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskMessagesResponseSerializer
    implements StructuredSerializer<TaskMessagesResponse> {
  @override
  final Iterable<Type> types = const [
    TaskMessagesResponse,
    _$TaskMessagesResponse
  ];
  @override
  final String wireName = 'TaskMessagesResponse';

  @override
  Iterable serialize(Serializers serializers, TaskMessagesResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
      'UNAUTHORIZED',
      serializers.serialize(object.unauthorized,
          specifiedType: const FullType(String)),
      'MESSAGES',
      serializers.serialize(object.messages,
          specifiedType:
              const FullType(BuiltList, const [const FullType(TaskMessage)])),
    ];
    if (object.currentUser != null) {
      result
        ..add('currentUser')
        ..add(serializers.serialize(object.currentUser,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  TaskMessagesResponse deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskMessagesResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAUTHORIZED':
          result.unauthorized = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'MESSAGES':
          result.messages.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(TaskMessage)]))
              as BuiltList);
          break;
        case 'currentUser':
          result.currentUser = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskMessage extends TaskMessage {
  @override
  final String id;
  @override
  final String dateTimeText;
  @override
  final String user;
  @override
  final String userText;
  @override
  final String text;
  @override
  final String attachement;

  factory _$TaskMessage([void updates(TaskMessageBuilder b)]) =>
      (new TaskMessageBuilder()..update(updates)).build();

  _$TaskMessage._(
      {this.id,
      this.dateTimeText,
      this.user,
      this.userText,
      this.text,
      this.attachement})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskMessage', 'id');
    }
    if (dateTimeText == null) {
      throw new BuiltValueNullFieldError('TaskMessage', 'dateTimeText');
    }
    if (user == null) {
      throw new BuiltValueNullFieldError('TaskMessage', 'user');
    }
    if (userText == null) {
      throw new BuiltValueNullFieldError('TaskMessage', 'userText');
    }
    if (text == null) {
      throw new BuiltValueNullFieldError('TaskMessage', 'text');
    }
    if (attachement == null) {
      throw new BuiltValueNullFieldError('TaskMessage', 'attachement');
    }
  }

  @override
  TaskMessage rebuild(void updates(TaskMessageBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskMessageBuilder toBuilder() => new TaskMessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskMessage &&
        id == other.id &&
        dateTimeText == other.dateTimeText &&
        user == other.user &&
        userText == other.userText &&
        text == other.text &&
        attachement == other.attachement;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, id.hashCode), dateTimeText.hashCode),
                    user.hashCode),
                userText.hashCode),
            text.hashCode),
        attachement.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskMessage')
          ..add('id', id)
          ..add('dateTimeText', dateTimeText)
          ..add('user', user)
          ..add('userText', userText)
          ..add('text', text)
          ..add('attachement', attachement))
        .toString();
  }
}

class TaskMessageBuilder implements Builder<TaskMessage, TaskMessageBuilder> {
  _$TaskMessage _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _dateTimeText;
  String get dateTimeText => _$this._dateTimeText;
  set dateTimeText(String dateTimeText) => _$this._dateTimeText = dateTimeText;

  String _user;
  String get user => _$this._user;
  set user(String user) => _$this._user = user;

  String _userText;
  String get userText => _$this._userText;
  set userText(String userText) => _$this._userText = userText;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  String _attachement;
  String get attachement => _$this._attachement;
  set attachement(String attachement) => _$this._attachement = attachement;

  TaskMessageBuilder();

  TaskMessageBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _dateTimeText = _$v.dateTimeText;
      _user = _$v.user;
      _userText = _$v.userText;
      _text = _$v.text;
      _attachement = _$v.attachement;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskMessage other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskMessage;
  }

  @override
  void update(void updates(TaskMessageBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskMessage build() {
    final _$result = _$v ??
        new _$TaskMessage._(
            id: id,
            dateTimeText: dateTimeText,
            user: user,
            userText: userText,
            text: text,
            attachement: attachement);
    replace(_$result);
    return _$result;
  }
}

class _$TaskMessagesRequest extends TaskMessagesRequest {
  @override
  final String token;
  @override
  final String uname;
  @override
  final String action;
  @override
  final String id;
  @override
  final String fileName;
  @override
  final String message;

  factory _$TaskMessagesRequest([void updates(TaskMessagesRequestBuilder b)]) =>
      (new TaskMessagesRequestBuilder()..update(updates)).build();

  _$TaskMessagesRequest._(
      {this.token,
      this.uname,
      this.action,
      this.id,
      this.fileName,
      this.message})
      : super._() {
    if (token == null) {
      throw new BuiltValueNullFieldError('TaskMessagesRequest', 'token');
    }
    if (uname == null) {
      throw new BuiltValueNullFieldError('TaskMessagesRequest', 'uname');
    }
    if (action == null) {
      throw new BuiltValueNullFieldError('TaskMessagesRequest', 'action');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskMessagesRequest', 'id');
    }
    if (fileName == null) {
      throw new BuiltValueNullFieldError('TaskMessagesRequest', 'fileName');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('TaskMessagesRequest', 'message');
    }
  }

  @override
  TaskMessagesRequest rebuild(void updates(TaskMessagesRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskMessagesRequestBuilder toBuilder() =>
      new TaskMessagesRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskMessagesRequest &&
        token == other.token &&
        uname == other.uname &&
        action == other.action &&
        id == other.id &&
        fileName == other.fileName &&
        message == other.message;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, token.hashCode), uname.hashCode),
                    action.hashCode),
                id.hashCode),
            fileName.hashCode),
        message.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskMessagesRequest')
          ..add('token', token)
          ..add('uname', uname)
          ..add('action', action)
          ..add('id', id)
          ..add('fileName', fileName)
          ..add('message', message))
        .toString();
  }
}

class TaskMessagesRequestBuilder
    implements Builder<TaskMessagesRequest, TaskMessagesRequestBuilder> {
  _$TaskMessagesRequest _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _uname;
  String get uname => _$this._uname;
  set uname(String uname) => _$this._uname = uname;

  String _action;
  String get action => _$this._action;
  set action(String action) => _$this._action = action;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _fileName;
  String get fileName => _$this._fileName;
  set fileName(String fileName) => _$this._fileName = fileName;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  TaskMessagesRequestBuilder();

  TaskMessagesRequestBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _uname = _$v.uname;
      _action = _$v.action;
      _id = _$v.id;
      _fileName = _$v.fileName;
      _message = _$v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskMessagesRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskMessagesRequest;
  }

  @override
  void update(void updates(TaskMessagesRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskMessagesRequest build() {
    final _$result = _$v ??
        new _$TaskMessagesRequest._(
            token: token,
            uname: uname,
            action: action,
            id: id,
            fileName: fileName,
            message: message);
    replace(_$result);
    return _$result;
  }
}

class _$TaskMessagesResponse extends TaskMessagesResponse {
  @override
  final String error;
  @override
  final String unauthorized;
  @override
  final BuiltList<TaskMessage> messages;
  @override
  final String currentUser;

  factory _$TaskMessagesResponse(
          [void updates(TaskMessagesResponseBuilder b)]) =>
      (new TaskMessagesResponseBuilder()..update(updates)).build();

  _$TaskMessagesResponse._(
      {this.error, this.unauthorized, this.messages, this.currentUser})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('TaskMessagesResponse', 'error');
    }
    if (unauthorized == null) {
      throw new BuiltValueNullFieldError(
          'TaskMessagesResponse', 'unauthorized');
    }
    if (messages == null) {
      throw new BuiltValueNullFieldError('TaskMessagesResponse', 'messages');
    }
  }

  @override
  TaskMessagesResponse rebuild(void updates(TaskMessagesResponseBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskMessagesResponseBuilder toBuilder() =>
      new TaskMessagesResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskMessagesResponse &&
        error == other.error &&
        unauthorized == other.unauthorized &&
        messages == other.messages &&
        currentUser == other.currentUser;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, error.hashCode), unauthorized.hashCode),
            messages.hashCode),
        currentUser.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskMessagesResponse')
          ..add('error', error)
          ..add('unauthorized', unauthorized)
          ..add('messages', messages)
          ..add('currentUser', currentUser))
        .toString();
  }
}

class TaskMessagesResponseBuilder
    implements Builder<TaskMessagesResponse, TaskMessagesResponseBuilder> {
  _$TaskMessagesResponse _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  String _unauthorized;
  String get unauthorized => _$this._unauthorized;
  set unauthorized(String unauthorized) => _$this._unauthorized = unauthorized;

  ListBuilder<TaskMessage> _messages;
  ListBuilder<TaskMessage> get messages =>
      _$this._messages ??= new ListBuilder<TaskMessage>();
  set messages(ListBuilder<TaskMessage> messages) =>
      _$this._messages = messages;

  String _currentUser;
  String get currentUser => _$this._currentUser;
  set currentUser(String currentUser) => _$this._currentUser = currentUser;

  TaskMessagesResponseBuilder();

  TaskMessagesResponseBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _unauthorized = _$v.unauthorized;
      _messages = _$v.messages?.toBuilder();
      _currentUser = _$v.currentUser;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskMessagesResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskMessagesResponse;
  }

  @override
  void update(void updates(TaskMessagesResponseBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskMessagesResponse build() {
    _$TaskMessagesResponse _$result;
    try {
      _$result = _$v ??
          new _$TaskMessagesResponse._(
              error: error,
              unauthorized: unauthorized,
              messages: messages.build(),
              currentUser: currentUser);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'messages';
        messages.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TaskMessagesResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}
