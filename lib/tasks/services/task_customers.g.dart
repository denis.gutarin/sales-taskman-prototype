// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_customers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<TaskCustomer> _$taskCustomerSerializer =
    new _$TaskCustomerSerializer();
Serializer<TaskCustomersRequest> _$taskCustomersRequestSerializer =
    new _$TaskCustomersRequestSerializer();
Serializer<TaskCustomersResponse> _$taskCustomersResponseSerializer =
    new _$TaskCustomersResponseSerializer();

class _$TaskCustomerSerializer implements StructuredSerializer<TaskCustomer> {
  @override
  final Iterable<Type> types = const [TaskCustomer, _$TaskCustomer];
  @override
  final String wireName = 'TaskCustomer';

  @override
  Iterable serialize(Serializers serializers, TaskCustomer object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'CUSTOMER',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'CUSTOMER_TEXT',
      serializers.serialize(object.text, specifiedType: const FullType(String)),
      'HOLDING_TEXT',
      serializers.serialize(object.holdingText,
          specifiedType: const FullType(String)),
      'CUSGROUP_TEXT',
      serializers.serialize(object.cusgroupText,
          specifiedType: const FullType(String)),
      'INN',
      serializers.serialize(object.inn, specifiedType: const FullType(String)),
      'SELECTED',
      serializers.serialize(object.selected,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskCustomer deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskCustomerBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'CUSTOMER':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CUSTOMER_TEXT':
          result.text = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'HOLDING_TEXT':
          result.holdingText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CUSGROUP_TEXT':
          result.cusgroupText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'INN':
          result.inn = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SELECTED':
          result.selected = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskCustomersRequestSerializer
    implements StructuredSerializer<TaskCustomersRequest> {
  @override
  final Iterable<Type> types = const [
    TaskCustomersRequest,
    _$TaskCustomersRequest
  ];
  @override
  final String wireName = 'TaskCustomersRequest';

  @override
  Iterable serialize(Serializers serializers, TaskCustomersRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'TOKEN',
      serializers.serialize(object.token,
          specifiedType: const FullType(String)),
      'UNAME',
      serializers.serialize(object.uname,
          specifiedType: const FullType(String)),
      'ACTION',
      serializers.serialize(object.action,
          specifiedType: const FullType(String)),
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'CUSTOMER',
      serializers.serialize(object.customer,
          specifiedType: const FullType(String)),
      'SEARCH',
      serializers.serialize(object.search,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskCustomersRequest deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskCustomersRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'TOKEN':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME':
          result.uname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ACTION':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CUSTOMER':
          result.customer = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SEARCH':
          result.search = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskCustomersResponseSerializer
    implements StructuredSerializer<TaskCustomersResponse> {
  @override
  final Iterable<Type> types = const [
    TaskCustomersResponse,
    _$TaskCustomersResponse
  ];
  @override
  final String wireName = 'TaskCustomersResponse';

  @override
  Iterable serialize(Serializers serializers, TaskCustomersResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
      'UNAUTHORIZED',
      serializers.serialize(object.unauthorized,
          specifiedType: const FullType(String)),
      'CUSTOMERS',
      serializers.serialize(object.customers,
          specifiedType:
              const FullType(BuiltList, const [const FullType(TaskCustomer)])),
    ];

    return result;
  }

  @override
  TaskCustomersResponse deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskCustomersResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAUTHORIZED':
          result.unauthorized = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CUSTOMERS':
          result.customers.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(TaskCustomer)]))
              as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$TaskCustomer extends TaskCustomer {
  @override
  final String id;
  @override
  final String text;
  @override
  final String holdingText;
  @override
  final String cusgroupText;
  @override
  final String inn;
  @override
  final String selected;

  factory _$TaskCustomer([void updates(TaskCustomerBuilder b)]) =>
      (new TaskCustomerBuilder()..update(updates)).build();

  _$TaskCustomer._(
      {this.id,
      this.text,
      this.holdingText,
      this.cusgroupText,
      this.inn,
      this.selected})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskCustomer', 'id');
    }
    if (text == null) {
      throw new BuiltValueNullFieldError('TaskCustomer', 'text');
    }
    if (holdingText == null) {
      throw new BuiltValueNullFieldError('TaskCustomer', 'holdingText');
    }
    if (cusgroupText == null) {
      throw new BuiltValueNullFieldError('TaskCustomer', 'cusgroupText');
    }
    if (inn == null) {
      throw new BuiltValueNullFieldError('TaskCustomer', 'inn');
    }
    if (selected == null) {
      throw new BuiltValueNullFieldError('TaskCustomer', 'selected');
    }
  }

  @override
  TaskCustomer rebuild(void updates(TaskCustomerBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskCustomerBuilder toBuilder() => new TaskCustomerBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskCustomer &&
        id == other.id &&
        text == other.text &&
        holdingText == other.holdingText &&
        cusgroupText == other.cusgroupText &&
        inn == other.inn &&
        selected == other.selected;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, id.hashCode), text.hashCode),
                    holdingText.hashCode),
                cusgroupText.hashCode),
            inn.hashCode),
        selected.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskCustomer')
          ..add('id', id)
          ..add('text', text)
          ..add('holdingText', holdingText)
          ..add('cusgroupText', cusgroupText)
          ..add('inn', inn)
          ..add('selected', selected))
        .toString();
  }
}

class TaskCustomerBuilder
    implements Builder<TaskCustomer, TaskCustomerBuilder> {
  _$TaskCustomer _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  String _holdingText;
  String get holdingText => _$this._holdingText;
  set holdingText(String holdingText) => _$this._holdingText = holdingText;

  String _cusgroupText;
  String get cusgroupText => _$this._cusgroupText;
  set cusgroupText(String cusgroupText) => _$this._cusgroupText = cusgroupText;

  String _inn;
  String get inn => _$this._inn;
  set inn(String inn) => _$this._inn = inn;

  String _selected;
  String get selected => _$this._selected;
  set selected(String selected) => _$this._selected = selected;

  TaskCustomerBuilder();

  TaskCustomerBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _text = _$v.text;
      _holdingText = _$v.holdingText;
      _cusgroupText = _$v.cusgroupText;
      _inn = _$v.inn;
      _selected = _$v.selected;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskCustomer other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskCustomer;
  }

  @override
  void update(void updates(TaskCustomerBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskCustomer build() {
    final _$result = _$v ??
        new _$TaskCustomer._(
            id: id,
            text: text,
            holdingText: holdingText,
            cusgroupText: cusgroupText,
            inn: inn,
            selected: selected);
    replace(_$result);
    return _$result;
  }
}

class _$TaskCustomersRequest extends TaskCustomersRequest {
  @override
  final String token;
  @override
  final String uname;
  @override
  final String action;
  @override
  final String id;
  @override
  final String customer;
  @override
  final String search;

  factory _$TaskCustomersRequest(
          [void updates(TaskCustomersRequestBuilder b)]) =>
      (new TaskCustomersRequestBuilder()..update(updates)).build();

  _$TaskCustomersRequest._(
      {this.token,
      this.uname,
      this.action,
      this.id,
      this.customer,
      this.search})
      : super._() {
    if (token == null) {
      throw new BuiltValueNullFieldError('TaskCustomersRequest', 'token');
    }
    if (uname == null) {
      throw new BuiltValueNullFieldError('TaskCustomersRequest', 'uname');
    }
    if (action == null) {
      throw new BuiltValueNullFieldError('TaskCustomersRequest', 'action');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskCustomersRequest', 'id');
    }
    if (customer == null) {
      throw new BuiltValueNullFieldError('TaskCustomersRequest', 'customer');
    }
    if (search == null) {
      throw new BuiltValueNullFieldError('TaskCustomersRequest', 'search');
    }
  }

  @override
  TaskCustomersRequest rebuild(void updates(TaskCustomersRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskCustomersRequestBuilder toBuilder() =>
      new TaskCustomersRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskCustomersRequest &&
        token == other.token &&
        uname == other.uname &&
        action == other.action &&
        id == other.id &&
        customer == other.customer &&
        search == other.search;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, token.hashCode), uname.hashCode),
                    action.hashCode),
                id.hashCode),
            customer.hashCode),
        search.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskCustomersRequest')
          ..add('token', token)
          ..add('uname', uname)
          ..add('action', action)
          ..add('id', id)
          ..add('customer', customer)
          ..add('search', search))
        .toString();
  }
}

class TaskCustomersRequestBuilder
    implements Builder<TaskCustomersRequest, TaskCustomersRequestBuilder> {
  _$TaskCustomersRequest _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _uname;
  String get uname => _$this._uname;
  set uname(String uname) => _$this._uname = uname;

  String _action;
  String get action => _$this._action;
  set action(String action) => _$this._action = action;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _customer;
  String get customer => _$this._customer;
  set customer(String customer) => _$this._customer = customer;

  String _search;
  String get search => _$this._search;
  set search(String search) => _$this._search = search;

  TaskCustomersRequestBuilder();

  TaskCustomersRequestBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _uname = _$v.uname;
      _action = _$v.action;
      _id = _$v.id;
      _customer = _$v.customer;
      _search = _$v.search;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskCustomersRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskCustomersRequest;
  }

  @override
  void update(void updates(TaskCustomersRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskCustomersRequest build() {
    final _$result = _$v ??
        new _$TaskCustomersRequest._(
            token: token,
            uname: uname,
            action: action,
            id: id,
            customer: customer,
            search: search);
    replace(_$result);
    return _$result;
  }
}

class _$TaskCustomersResponse extends TaskCustomersResponse {
  @override
  final String error;
  @override
  final String unauthorized;
  @override
  final BuiltList<TaskCustomer> customers;

  factory _$TaskCustomersResponse(
          [void updates(TaskCustomersResponseBuilder b)]) =>
      (new TaskCustomersResponseBuilder()..update(updates)).build();

  _$TaskCustomersResponse._({this.error, this.unauthorized, this.customers})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('TaskCustomersResponse', 'error');
    }
    if (unauthorized == null) {
      throw new BuiltValueNullFieldError(
          'TaskCustomersResponse', 'unauthorized');
    }
    if (customers == null) {
      throw new BuiltValueNullFieldError('TaskCustomersResponse', 'customers');
    }
  }

  @override
  TaskCustomersResponse rebuild(void updates(TaskCustomersResponseBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskCustomersResponseBuilder toBuilder() =>
      new TaskCustomersResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskCustomersResponse &&
        error == other.error &&
        unauthorized == other.unauthorized &&
        customers == other.customers;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, error.hashCode), unauthorized.hashCode),
        customers.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskCustomersResponse')
          ..add('error', error)
          ..add('unauthorized', unauthorized)
          ..add('customers', customers))
        .toString();
  }
}

class TaskCustomersResponseBuilder
    implements Builder<TaskCustomersResponse, TaskCustomersResponseBuilder> {
  _$TaskCustomersResponse _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  String _unauthorized;
  String get unauthorized => _$this._unauthorized;
  set unauthorized(String unauthorized) => _$this._unauthorized = unauthorized;

  ListBuilder<TaskCustomer> _customers;
  ListBuilder<TaskCustomer> get customers =>
      _$this._customers ??= new ListBuilder<TaskCustomer>();
  set customers(ListBuilder<TaskCustomer> customers) =>
      _$this._customers = customers;

  TaskCustomersResponseBuilder();

  TaskCustomersResponseBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _unauthorized = _$v.unauthorized;
      _customers = _$v.customers?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskCustomersResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskCustomersResponse;
  }

  @override
  void update(void updates(TaskCustomersResponseBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskCustomersResponse build() {
    _$TaskCustomersResponse _$result;
    try {
      _$result = _$v ??
          new _$TaskCustomersResponse._(
              error: error,
              unauthorized: unauthorized,
              customers: customers.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'customers';
        customers.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TaskCustomersResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}
