// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_constructions.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<TaskConstruction> _$taskConstructionSerializer =
    new _$TaskConstructionSerializer();
Serializer<TaskConstructionsRequest> _$taskConstructionsRequestSerializer =
    new _$TaskConstructionsRequestSerializer();
Serializer<TaskConstructionsResponse> _$taskConstructionsResponseSerializer =
    new _$TaskConstructionsResponseSerializer();

class _$TaskConstructionSerializer
    implements StructuredSerializer<TaskConstruction> {
  @override
  final Iterable<Type> types = const [TaskConstruction, _$TaskConstruction];
  @override
  final String wireName = 'TaskConstruction';

  @override
  Iterable serialize(Serializers serializers, TaskConstruction object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'CONSTRUCTION',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'CONSTRUCTION_TEXT',
      serializers.serialize(object.text, specifiedType: const FullType(String)),
      'SELECTED',
      serializers.serialize(object.selected,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskConstruction deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskConstructionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'CONSTRUCTION':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CONSTRUCTION_TEXT':
          result.text = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SELECTED':
          result.selected = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskConstructionsRequestSerializer
    implements StructuredSerializer<TaskConstructionsRequest> {
  @override
  final Iterable<Type> types = const [
    TaskConstructionsRequest,
    _$TaskConstructionsRequest
  ];
  @override
  final String wireName = 'TaskConstructionsRequest';

  @override
  Iterable serialize(Serializers serializers, TaskConstructionsRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'TOKEN',
      serializers.serialize(object.token,
          specifiedType: const FullType(String)),
      'UNAME',
      serializers.serialize(object.uname,
          specifiedType: const FullType(String)),
      'ACTION',
      serializers.serialize(object.action,
          specifiedType: const FullType(String)),
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'CONSTRUCTION',
      serializers.serialize(object.construction,
          specifiedType: const FullType(String)),
      'SEARCH',
      serializers.serialize(object.search,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskConstructionsRequest deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskConstructionsRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'TOKEN':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME':
          result.uname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ACTION':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CONSTRUCTION':
          result.construction = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SEARCH':
          result.search = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskConstructionsResponseSerializer
    implements StructuredSerializer<TaskConstructionsResponse> {
  @override
  final Iterable<Type> types = const [
    TaskConstructionsResponse,
    _$TaskConstructionsResponse
  ];
  @override
  final String wireName = 'TaskConstructionsResponse';

  @override
  Iterable serialize(Serializers serializers, TaskConstructionsResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
      'UNAUTHORIZED',
      serializers.serialize(object.unauthorized,
          specifiedType: const FullType(String)),
      'CONSTRUCTIONS',
      serializers.serialize(object.constructions,
          specifiedType: const FullType(
              BuiltList, const [const FullType(TaskConstruction)])),
    ];

    return result;
  }

  @override
  TaskConstructionsResponse deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskConstructionsResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAUTHORIZED':
          result.unauthorized = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CONSTRUCTIONS':
          result.constructions.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(TaskConstruction)]))
              as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$TaskConstruction extends TaskConstruction {
  @override
  final String id;
  @override
  final String text;
  @override
  final String selected;

  factory _$TaskConstruction([void updates(TaskConstructionBuilder b)]) =>
      (new TaskConstructionBuilder()..update(updates)).build();

  _$TaskConstruction._({this.id, this.text, this.selected}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskConstruction', 'id');
    }
    if (text == null) {
      throw new BuiltValueNullFieldError('TaskConstruction', 'text');
    }
    if (selected == null) {
      throw new BuiltValueNullFieldError('TaskConstruction', 'selected');
    }
  }

  @override
  TaskConstruction rebuild(void updates(TaskConstructionBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskConstructionBuilder toBuilder() =>
      new TaskConstructionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskConstruction &&
        id == other.id &&
        text == other.text &&
        selected == other.selected;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), text.hashCode), selected.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskConstruction')
          ..add('id', id)
          ..add('text', text)
          ..add('selected', selected))
        .toString();
  }
}

class TaskConstructionBuilder
    implements Builder<TaskConstruction, TaskConstructionBuilder> {
  _$TaskConstruction _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  String _selected;
  String get selected => _$this._selected;
  set selected(String selected) => _$this._selected = selected;

  TaskConstructionBuilder();

  TaskConstructionBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _text = _$v.text;
      _selected = _$v.selected;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskConstruction other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskConstruction;
  }

  @override
  void update(void updates(TaskConstructionBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskConstruction build() {
    final _$result =
        _$v ?? new _$TaskConstruction._(id: id, text: text, selected: selected);
    replace(_$result);
    return _$result;
  }
}

class _$TaskConstructionsRequest extends TaskConstructionsRequest {
  @override
  final String token;
  @override
  final String uname;
  @override
  final String action;
  @override
  final String id;
  @override
  final String construction;
  @override
  final String search;

  factory _$TaskConstructionsRequest(
          [void updates(TaskConstructionsRequestBuilder b)]) =>
      (new TaskConstructionsRequestBuilder()..update(updates)).build();

  _$TaskConstructionsRequest._(
      {this.token,
      this.uname,
      this.action,
      this.id,
      this.construction,
      this.search})
      : super._() {
    if (token == null) {
      throw new BuiltValueNullFieldError('TaskConstructionsRequest', 'token');
    }
    if (uname == null) {
      throw new BuiltValueNullFieldError('TaskConstructionsRequest', 'uname');
    }
    if (action == null) {
      throw new BuiltValueNullFieldError('TaskConstructionsRequest', 'action');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskConstructionsRequest', 'id');
    }
    if (construction == null) {
      throw new BuiltValueNullFieldError(
          'TaskConstructionsRequest', 'construction');
    }
    if (search == null) {
      throw new BuiltValueNullFieldError('TaskConstructionsRequest', 'search');
    }
  }

  @override
  TaskConstructionsRequest rebuild(
          void updates(TaskConstructionsRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskConstructionsRequestBuilder toBuilder() =>
      new TaskConstructionsRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskConstructionsRequest &&
        token == other.token &&
        uname == other.uname &&
        action == other.action &&
        id == other.id &&
        construction == other.construction &&
        search == other.search;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, token.hashCode), uname.hashCode),
                    action.hashCode),
                id.hashCode),
            construction.hashCode),
        search.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskConstructionsRequest')
          ..add('token', token)
          ..add('uname', uname)
          ..add('action', action)
          ..add('id', id)
          ..add('construction', construction)
          ..add('search', search))
        .toString();
  }
}

class TaskConstructionsRequestBuilder
    implements
        Builder<TaskConstructionsRequest, TaskConstructionsRequestBuilder> {
  _$TaskConstructionsRequest _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _uname;
  String get uname => _$this._uname;
  set uname(String uname) => _$this._uname = uname;

  String _action;
  String get action => _$this._action;
  set action(String action) => _$this._action = action;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _construction;
  String get construction => _$this._construction;
  set construction(String construction) => _$this._construction = construction;

  String _search;
  String get search => _$this._search;
  set search(String search) => _$this._search = search;

  TaskConstructionsRequestBuilder();

  TaskConstructionsRequestBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _uname = _$v.uname;
      _action = _$v.action;
      _id = _$v.id;
      _construction = _$v.construction;
      _search = _$v.search;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskConstructionsRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskConstructionsRequest;
  }

  @override
  void update(void updates(TaskConstructionsRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskConstructionsRequest build() {
    final _$result = _$v ??
        new _$TaskConstructionsRequest._(
            token: token,
            uname: uname,
            action: action,
            id: id,
            construction: construction,
            search: search);
    replace(_$result);
    return _$result;
  }
}

class _$TaskConstructionsResponse extends TaskConstructionsResponse {
  @override
  final String error;
  @override
  final String unauthorized;
  @override
  final BuiltList<TaskConstruction> constructions;

  factory _$TaskConstructionsResponse(
          [void updates(TaskConstructionsResponseBuilder b)]) =>
      (new TaskConstructionsResponseBuilder()..update(updates)).build();

  _$TaskConstructionsResponse._(
      {this.error, this.unauthorized, this.constructions})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('TaskConstructionsResponse', 'error');
    }
    if (unauthorized == null) {
      throw new BuiltValueNullFieldError(
          'TaskConstructionsResponse', 'unauthorized');
    }
    if (constructions == null) {
      throw new BuiltValueNullFieldError(
          'TaskConstructionsResponse', 'constructions');
    }
  }

  @override
  TaskConstructionsResponse rebuild(
          void updates(TaskConstructionsResponseBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskConstructionsResponseBuilder toBuilder() =>
      new TaskConstructionsResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskConstructionsResponse &&
        error == other.error &&
        unauthorized == other.unauthorized &&
        constructions == other.constructions;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, error.hashCode), unauthorized.hashCode),
        constructions.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskConstructionsResponse')
          ..add('error', error)
          ..add('unauthorized', unauthorized)
          ..add('constructions', constructions))
        .toString();
  }
}

class TaskConstructionsResponseBuilder
    implements
        Builder<TaskConstructionsResponse, TaskConstructionsResponseBuilder> {
  _$TaskConstructionsResponse _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  String _unauthorized;
  String get unauthorized => _$this._unauthorized;
  set unauthorized(String unauthorized) => _$this._unauthorized = unauthorized;

  ListBuilder<TaskConstruction> _constructions;
  ListBuilder<TaskConstruction> get constructions =>
      _$this._constructions ??= new ListBuilder<TaskConstruction>();
  set constructions(ListBuilder<TaskConstruction> constructions) =>
      _$this._constructions = constructions;

  TaskConstructionsResponseBuilder();

  TaskConstructionsResponseBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _unauthorized = _$v.unauthorized;
      _constructions = _$v.constructions?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskConstructionsResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskConstructionsResponse;
  }

  @override
  void update(void updates(TaskConstructionsResponseBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskConstructionsResponse build() {
    _$TaskConstructionsResponse _$result;
    try {
      _$result = _$v ??
          new _$TaskConstructionsResponse._(
              error: error,
              unauthorized: unauthorized,
              constructions: constructions.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'constructions';
        constructions.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TaskConstructionsResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}
