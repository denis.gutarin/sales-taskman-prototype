import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/tasks/services/serializers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'task_customers.g.dart';

abstract class TaskCustomer
    implements Built<TaskCustomer, TaskCustomerBuilder> {
  TaskCustomer._();
  factory TaskCustomer([updates(TaskCustomerBuilder b)]) = _$TaskCustomer;

  @BuiltValueField(wireName: 'CUSTOMER')
  String get id;

  @BuiltValueField(wireName: 'CUSTOMER_TEXT')
  String get text;

  @BuiltValueField(wireName: 'HOLDING_TEXT')
  String get holdingText;

  @BuiltValueField(wireName: 'CUSGROUP_TEXT')
  String get cusgroupText;

  @BuiltValueField(wireName: 'INN')
  String get inn;

  @BuiltValueField(wireName: 'SELECTED')
  String get selected;

  static Serializer<TaskCustomer> get serializer => _$taskCustomerSerializer;
}

abstract class TaskCustomersRequest
    implements Built<TaskCustomersRequest, TaskCustomersRequestBuilder> {
  TaskCustomersRequest._();
  factory TaskCustomersRequest([updates(TaskCustomersRequestBuilder b)]) =
      _$TaskCustomersRequest;

  @BuiltValueField(wireName: "TOKEN")
  String get token;

  @BuiltValueField(wireName: "UNAME")
  String get uname;

  @BuiltValueField(wireName: "ACTION")
  String get action;

  @BuiltValueField(wireName: "ID")
  String get id;

  @BuiltValueField(wireName: 'CUSTOMER')
  String get customer;

  @BuiltValueField(wireName: 'SEARCH')
  String get search;

  static Serializer<TaskCustomersRequest> get serializer =>
      _$taskCustomersRequestSerializer;
}

abstract class TaskCustomersResponse
    implements Built<TaskCustomersResponse, TaskCustomersResponseBuilder> {
  TaskCustomersResponse._();

  @BuiltValueField(wireName: 'ERROR')
  String get error;

  @BuiltValueField(wireName: 'UNAUTHORIZED')
  String get unauthorized;

  @BuiltValueField(wireName: 'CUSTOMERS')
  BuiltList<TaskCustomer> get customers;

  factory TaskCustomersResponse([updates(TaskCustomersResponseBuilder b)]) =
      _$TaskCustomersResponse;

  static Serializer<TaskCustomersResponse> get serializer =>
      _$taskCustomersResponseSerializer;
}

Future<TaskCustomersResponse> callTaskCustomers(
    TaskCustomersRequest input) async {
  if (input.action == 'LOADING') return null;
  final saved = await SharedPreferences.getInstance();
  final newInput = input.rebuild((it) => it
    ..uname = saved.getString("uname")
    ..token = saved.getString("token"));
  final String body = json.encode(
      serializers.serializeWith(TaskCustomersRequest.serializer, newInput));
  final resp = await http.post(TASKCUSTOMERS_URL, body: body);
  final result = serializers.deserializeWith(
      TaskCustomersResponse.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);

  return result;
}
