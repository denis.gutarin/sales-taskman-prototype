import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:ironman/tasks/services/task_list.dart';
import 'package:ironman/tasks/services/task_info.dart';
import 'package:ironman/tasks/services/task_messages.dart';
import 'package:ironman/tasks/services/task_customers.dart';
import 'package:ironman/tasks/services/task_constructions.dart';
import 'package:ironman/tasks/services/task_executors.dart';
import 'package:ironman/tasks/services/task_datetime.dart';
import 'package:ironman/tasks/services/task_files.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  TaskList,
  TaskListRequest,
  TaskInfo,
  TaskInfoRequest,
  TaskInfoResponse,
  TaskMessage,
  TaskMessagesRequest,
  TaskMessagesResponse,
  TaskCustomer,
  TaskCustomersRequest,
  TaskCustomersResponse,
  TaskConstruction,
  TaskConstructionsRequest,
  TaskConstructionsResponse,
  TaskExecutor,
  TaskExecutorsRequest,
  TaskExecutorsResponse,
  TaskDatetimeRequest,
  TaskDatetimeResponse,
  TaskFilesRequest,
  TaskFilesResponse,
])
final Serializers serializers = (_$serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin())
      ..add(Iso8601DateTimeSerializer()))
    .build();
