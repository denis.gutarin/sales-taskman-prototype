// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(TaskConstruction.serializer)
      ..add(TaskConstructionsRequest.serializer)
      ..add(TaskConstructionsResponse.serializer)
      ..add(TaskCustomer.serializer)
      ..add(TaskCustomersRequest.serializer)
      ..add(TaskCustomersResponse.serializer)
      ..add(TaskDatetimeRequest.serializer)
      ..add(TaskDatetimeResponse.serializer)
      ..add(TaskExecutor.serializer)
      ..add(TaskExecutorsRequest.serializer)
      ..add(TaskExecutorsResponse.serializer)
      ..add(TaskFilesRequest.serializer)
      ..add(TaskFilesResponse.serializer)
      ..add(TaskInfo.serializer)
      ..add(TaskInfoRequest.serializer)
      ..add(TaskInfoResponse.serializer)
      ..add(TaskList.serializer)
      ..add(TaskListRequest.serializer)
      ..add(TaskMessage.serializer)
      ..add(TaskMessagesRequest.serializer)
      ..add(TaskMessagesResponse.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TaskConstruction)]),
          () => new ListBuilder<TaskConstruction>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TaskCustomer)]),
          () => new ListBuilder<TaskCustomer>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TaskExecutor)]),
          () => new ListBuilder<TaskExecutor>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TaskInfo)]),
          () => new ListBuilder<TaskInfo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TaskMessage)]),
          () => new ListBuilder<TaskMessage>()))
    .build();
