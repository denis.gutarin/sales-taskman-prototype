// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_info.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<TaskInfoRequest> _$taskInfoRequestSerializer =
    new _$TaskInfoRequestSerializer();
Serializer<TaskInfoResponse> _$taskInfoResponseSerializer =
    new _$TaskInfoResponseSerializer();
Serializer<TaskInfo> _$taskInfoSerializer = new _$TaskInfoSerializer();

class _$TaskInfoRequestSerializer
    implements StructuredSerializer<TaskInfoRequest> {
  @override
  final Iterable<Type> types = const [TaskInfoRequest, _$TaskInfoRequest];
  @override
  final String wireName = 'TaskInfoRequest';

  @override
  Iterable serialize(Serializers serializers, TaskInfoRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'TOKEN',
      serializers.serialize(object.token,
          specifiedType: const FullType(String)),
      'UNAME',
      serializers.serialize(object.uname,
          specifiedType: const FullType(String)),
      'ACTION',
      serializers.serialize(object.action,
          specifiedType: const FullType(String)),
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskInfoRequest deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskInfoRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'TOKEN':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME':
          result.uname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ACTION':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskInfoResponseSerializer
    implements StructuredSerializer<TaskInfoResponse> {
  @override
  final Iterable<Type> types = const [TaskInfoResponse, _$TaskInfoResponse];
  @override
  final String wireName = 'TaskInfoResponse';

  @override
  Iterable serialize(Serializers serializers, TaskInfoResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
      'UNAUTHORIZED',
      serializers.serialize(object.unauthorized,
          specifiedType: const FullType(String)),
      'TASK',
      serializers.serialize(object.task,
          specifiedType: const FullType(TaskInfo)),
    ];

    return result;
  }

  @override
  TaskInfoResponse deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskInfoResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAUTHORIZED':
          result.unauthorized = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'TASK':
          result.task.replace(serializers.deserialize(value,
              specifiedType: const FullType(TaskInfo)) as TaskInfo);
          break;
      }
    }

    return result.build();
  }
}

class _$TaskInfoSerializer implements StructuredSerializer<TaskInfo> {
  @override
  final Iterable<Type> types = const [TaskInfo, _$TaskInfo];
  @override
  final String wireName = 'TaskInfo';

  @override
  Iterable serialize(Serializers serializers, TaskInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'AUTHOR_ID',
      serializers.serialize(object.authorId,
          specifiedType: const FullType(String)),
      'AUTHOR_TEXT',
      serializers.serialize(object.authorText,
          specifiedType: const FullType(String)),
      'CREATED_AT',
      serializers.serialize(object.createdAt,
          specifiedType: const FullType(DateTime)),
      'EXECUTOR_ID',
      serializers.serialize(object.executorId,
          specifiedType: const FullType(String)),
      'EXECUTOR_TEXT',
      serializers.serialize(object.executorText,
          specifiedType: const FullType(String)),
      'DEADLINE',
      serializers.serialize(object.deadline,
          specifiedType: const FullType(DateTime)),
      'DONE',
      serializers.serialize(object.done, specifiedType: const FullType(String)),
      'CUSTOMER_ID',
      serializers.serialize(object.customerId,
          specifiedType: const FullType(String)),
      'CUSTOMER_TEXT',
      serializers.serialize(object.customerText,
          specifiedType: const FullType(String)),
      'CONSTRUCTION_ID',
      serializers.serialize(object.constructionId,
          specifiedType: const FullType(String)),
      'CONSTRUCTION_TEXT',
      serializers.serialize(object.constructionText,
          specifiedType: const FullType(String)),
      'FIRST_MESSAGE',
      serializers.serialize(object.firstMessage,
          specifiedType: const FullType(String)),
      'LAST_MESSAGE',
      serializers.serialize(object.lastMessage,
          specifiedType: const FullType(String)),
    ];
    if (object.currentUser != null) {
      result
        ..add('currentUser')
        ..add(serializers.serialize(object.currentUser,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  TaskInfo deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'AUTHOR_ID':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'AUTHOR_TEXT':
          result.authorText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CREATED_AT':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'EXECUTOR_ID':
          result.executorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'EXECUTOR_TEXT':
          result.executorText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'DEADLINE':
          result.deadline = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'DONE':
          result.done = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CUSTOMER_ID':
          result.customerId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CUSTOMER_TEXT':
          result.customerText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CONSTRUCTION_ID':
          result.constructionId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'CONSTRUCTION_TEXT':
          result.constructionText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'FIRST_MESSAGE':
          result.firstMessage = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'LAST_MESSAGE':
          result.lastMessage = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'currentUser':
          result.currentUser = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskInfoRequest extends TaskInfoRequest {
  @override
  final String token;
  @override
  final String uname;
  @override
  final String action;
  @override
  final String id;

  factory _$TaskInfoRequest([void updates(TaskInfoRequestBuilder b)]) =>
      (new TaskInfoRequestBuilder()..update(updates)).build();

  _$TaskInfoRequest._({this.token, this.uname, this.action, this.id})
      : super._() {
    if (token == null) {
      throw new BuiltValueNullFieldError('TaskInfoRequest', 'token');
    }
    if (uname == null) {
      throw new BuiltValueNullFieldError('TaskInfoRequest', 'uname');
    }
    if (action == null) {
      throw new BuiltValueNullFieldError('TaskInfoRequest', 'action');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskInfoRequest', 'id');
    }
  }

  @override
  TaskInfoRequest rebuild(void updates(TaskInfoRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskInfoRequestBuilder toBuilder() =>
      new TaskInfoRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskInfoRequest &&
        token == other.token &&
        uname == other.uname &&
        action == other.action &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, token.hashCode), uname.hashCode), action.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskInfoRequest')
          ..add('token', token)
          ..add('uname', uname)
          ..add('action', action)
          ..add('id', id))
        .toString();
  }
}

class TaskInfoRequestBuilder
    implements Builder<TaskInfoRequest, TaskInfoRequestBuilder> {
  _$TaskInfoRequest _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _uname;
  String get uname => _$this._uname;
  set uname(String uname) => _$this._uname = uname;

  String _action;
  String get action => _$this._action;
  set action(String action) => _$this._action = action;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  TaskInfoRequestBuilder();

  TaskInfoRequestBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _uname = _$v.uname;
      _action = _$v.action;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskInfoRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskInfoRequest;
  }

  @override
  void update(void updates(TaskInfoRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskInfoRequest build() {
    final _$result = _$v ??
        new _$TaskInfoRequest._(
            token: token, uname: uname, action: action, id: id);
    replace(_$result);
    return _$result;
  }
}

class _$TaskInfoResponse extends TaskInfoResponse {
  @override
  final String error;
  @override
  final String unauthorized;
  @override
  final TaskInfo task;

  factory _$TaskInfoResponse([void updates(TaskInfoResponseBuilder b)]) =>
      (new TaskInfoResponseBuilder()..update(updates)).build();

  _$TaskInfoResponse._({this.error, this.unauthorized, this.task}) : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('TaskInfoResponse', 'error');
    }
    if (unauthorized == null) {
      throw new BuiltValueNullFieldError('TaskInfoResponse', 'unauthorized');
    }
    if (task == null) {
      throw new BuiltValueNullFieldError('TaskInfoResponse', 'task');
    }
  }

  @override
  TaskInfoResponse rebuild(void updates(TaskInfoResponseBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskInfoResponseBuilder toBuilder() =>
      new TaskInfoResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskInfoResponse &&
        error == other.error &&
        unauthorized == other.unauthorized &&
        task == other.task;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, error.hashCode), unauthorized.hashCode), task.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskInfoResponse')
          ..add('error', error)
          ..add('unauthorized', unauthorized)
          ..add('task', task))
        .toString();
  }
}

class TaskInfoResponseBuilder
    implements Builder<TaskInfoResponse, TaskInfoResponseBuilder> {
  _$TaskInfoResponse _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  String _unauthorized;
  String get unauthorized => _$this._unauthorized;
  set unauthorized(String unauthorized) => _$this._unauthorized = unauthorized;

  TaskInfoBuilder _task;
  TaskInfoBuilder get task => _$this._task ??= new TaskInfoBuilder();
  set task(TaskInfoBuilder task) => _$this._task = task;

  TaskInfoResponseBuilder();

  TaskInfoResponseBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _unauthorized = _$v.unauthorized;
      _task = _$v.task?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskInfoResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskInfoResponse;
  }

  @override
  void update(void updates(TaskInfoResponseBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskInfoResponse build() {
    _$TaskInfoResponse _$result;
    try {
      _$result = _$v ??
          new _$TaskInfoResponse._(
              error: error, unauthorized: unauthorized, task: task.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'task';
        task.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TaskInfoResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$TaskInfo extends TaskInfo {
  @override
  final String id;
  @override
  final String authorId;
  @override
  final String authorText;
  @override
  final DateTime createdAt;
  @override
  final String executorId;
  @override
  final String executorText;
  @override
  final DateTime deadline;
  @override
  final String done;
  @override
  final String customerId;
  @override
  final String customerText;
  @override
  final String constructionId;
  @override
  final String constructionText;
  @override
  final String firstMessage;
  @override
  final String lastMessage;
  @override
  final String currentUser;

  factory _$TaskInfo([void updates(TaskInfoBuilder b)]) =>
      (new TaskInfoBuilder()..update(updates)).build();

  _$TaskInfo._(
      {this.id,
      this.authorId,
      this.authorText,
      this.createdAt,
      this.executorId,
      this.executorText,
      this.deadline,
      this.done,
      this.customerId,
      this.customerText,
      this.constructionId,
      this.constructionText,
      this.firstMessage,
      this.lastMessage,
      this.currentUser})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'id');
    }
    if (authorId == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'authorId');
    }
    if (authorText == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'authorText');
    }
    if (createdAt == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'createdAt');
    }
    if (executorId == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'executorId');
    }
    if (executorText == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'executorText');
    }
    if (deadline == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'deadline');
    }
    if (done == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'done');
    }
    if (customerId == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'customerId');
    }
    if (customerText == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'customerText');
    }
    if (constructionId == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'constructionId');
    }
    if (constructionText == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'constructionText');
    }
    if (firstMessage == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'firstMessage');
    }
    if (lastMessage == null) {
      throw new BuiltValueNullFieldError('TaskInfo', 'lastMessage');
    }
  }

  @override
  TaskInfo rebuild(void updates(TaskInfoBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskInfoBuilder toBuilder() => new TaskInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskInfo &&
        id == other.id &&
        authorId == other.authorId &&
        authorText == other.authorText &&
        createdAt == other.createdAt &&
        executorId == other.executorId &&
        executorText == other.executorText &&
        deadline == other.deadline &&
        done == other.done &&
        customerId == other.customerId &&
        customerText == other.customerText &&
        constructionId == other.constructionId &&
        constructionText == other.constructionText &&
        firstMessage == other.firstMessage &&
        lastMessage == other.lastMessage &&
        currentUser == other.currentUser;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc($jc(0, id.hashCode),
                                                            authorId.hashCode),
                                                        authorText.hashCode),
                                                    createdAt.hashCode),
                                                executorId.hashCode),
                                            executorText.hashCode),
                                        deadline.hashCode),
                                    done.hashCode),
                                customerId.hashCode),
                            customerText.hashCode),
                        constructionId.hashCode),
                    constructionText.hashCode),
                firstMessage.hashCode),
            lastMessage.hashCode),
        currentUser.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskInfo')
          ..add('id', id)
          ..add('authorId', authorId)
          ..add('authorText', authorText)
          ..add('createdAt', createdAt)
          ..add('executorId', executorId)
          ..add('executorText', executorText)
          ..add('deadline', deadline)
          ..add('done', done)
          ..add('customerId', customerId)
          ..add('customerText', customerText)
          ..add('constructionId', constructionId)
          ..add('constructionText', constructionText)
          ..add('firstMessage', firstMessage)
          ..add('lastMessage', lastMessage)
          ..add('currentUser', currentUser))
        .toString();
  }
}

class TaskInfoBuilder implements Builder<TaskInfo, TaskInfoBuilder> {
  _$TaskInfo _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _authorId;
  String get authorId => _$this._authorId;
  set authorId(String authorId) => _$this._authorId = authorId;

  String _authorText;
  String get authorText => _$this._authorText;
  set authorText(String authorText) => _$this._authorText = authorText;

  DateTime _createdAt;
  DateTime get createdAt => _$this._createdAt;
  set createdAt(DateTime createdAt) => _$this._createdAt = createdAt;

  String _executorId;
  String get executorId => _$this._executorId;
  set executorId(String executorId) => _$this._executorId = executorId;

  String _executorText;
  String get executorText => _$this._executorText;
  set executorText(String executorText) => _$this._executorText = executorText;

  DateTime _deadline;
  DateTime get deadline => _$this._deadline;
  set deadline(DateTime deadline) => _$this._deadline = deadline;

  String _done;
  String get done => _$this._done;
  set done(String done) => _$this._done = done;

  String _customerId;
  String get customerId => _$this._customerId;
  set customerId(String customerId) => _$this._customerId = customerId;

  String _customerText;
  String get customerText => _$this._customerText;
  set customerText(String customerText) => _$this._customerText = customerText;

  String _constructionId;
  String get constructionId => _$this._constructionId;
  set constructionId(String constructionId) =>
      _$this._constructionId = constructionId;

  String _constructionText;
  String get constructionText => _$this._constructionText;
  set constructionText(String constructionText) =>
      _$this._constructionText = constructionText;

  String _firstMessage;
  String get firstMessage => _$this._firstMessage;
  set firstMessage(String firstMessage) => _$this._firstMessage = firstMessage;

  String _lastMessage;
  String get lastMessage => _$this._lastMessage;
  set lastMessage(String lastMessage) => _$this._lastMessage = lastMessage;

  String _currentUser;
  String get currentUser => _$this._currentUser;
  set currentUser(String currentUser) => _$this._currentUser = currentUser;

  TaskInfoBuilder();

  TaskInfoBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _authorId = _$v.authorId;
      _authorText = _$v.authorText;
      _createdAt = _$v.createdAt;
      _executorId = _$v.executorId;
      _executorText = _$v.executorText;
      _deadline = _$v.deadline;
      _done = _$v.done;
      _customerId = _$v.customerId;
      _customerText = _$v.customerText;
      _constructionId = _$v.constructionId;
      _constructionText = _$v.constructionText;
      _firstMessage = _$v.firstMessage;
      _lastMessage = _$v.lastMessage;
      _currentUser = _$v.currentUser;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskInfo other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskInfo;
  }

  @override
  void update(void updates(TaskInfoBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskInfo build() {
    final _$result = _$v ??
        new _$TaskInfo._(
            id: id,
            authorId: authorId,
            authorText: authorText,
            createdAt: createdAt,
            executorId: executorId,
            executorText: executorText,
            deadline: deadline,
            done: done,
            customerId: customerId,
            customerText: customerText,
            constructionId: constructionId,
            constructionText: constructionText,
            firstMessage: firstMessage,
            lastMessage: lastMessage,
            currentUser: currentUser);
    replace(_$result);
    return _$result;
  }
}
