import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/tasks/services/serializers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'task_constructions.g.dart';

abstract class TaskConstruction
    implements Built<TaskConstruction, TaskConstructionBuilder> {
  TaskConstruction._();
  factory TaskConstruction([updates(TaskConstructionBuilder b)]) =
      _$TaskConstruction;

  @BuiltValueField(wireName: 'CONSTRUCTION')
  String get id;

  @BuiltValueField(wireName: 'CONSTRUCTION_TEXT')
  String get text;

  @BuiltValueField(wireName: 'SELECTED')
  String get selected;

  static Serializer<TaskConstruction> get serializer =>
      _$taskConstructionSerializer;
}

abstract class TaskConstructionsRequest
    implements
        Built<TaskConstructionsRequest, TaskConstructionsRequestBuilder> {
  TaskConstructionsRequest._();
  factory TaskConstructionsRequest(
          [updates(TaskConstructionsRequestBuilder b)]) =
      _$TaskConstructionsRequest;

  @BuiltValueField(wireName: "TOKEN")
  String get token;

  @BuiltValueField(wireName: "UNAME")
  String get uname;

  @BuiltValueField(wireName: "ACTION")
  String get action;

  @BuiltValueField(wireName: "ID")
  String get id;

  @BuiltValueField(wireName: 'CONSTRUCTION')
  String get construction;

  @BuiltValueField(wireName: 'SEARCH')
  String get search;

  static Serializer<TaskConstructionsRequest> get serializer =>
      _$taskConstructionsRequestSerializer;
}

abstract class TaskConstructionsResponse
    implements
        Built<TaskConstructionsResponse, TaskConstructionsResponseBuilder> {
  TaskConstructionsResponse._();

  @BuiltValueField(wireName: 'ERROR')
  String get error;

  @BuiltValueField(wireName: 'UNAUTHORIZED')
  String get unauthorized;

  @BuiltValueField(wireName: 'CONSTRUCTIONS')
  BuiltList<TaskConstruction> get constructions;

  factory TaskConstructionsResponse(
          [updates(TaskConstructionsResponseBuilder b)]) =
      _$TaskConstructionsResponse;

  static Serializer<TaskConstructionsResponse> get serializer =>
      _$taskConstructionsResponseSerializer;
}

Future<TaskConstructionsResponse> callTaskConstructions(
    TaskConstructionsRequest input) async {
  if (input.action == 'LOADING') return null;
  final saved = await SharedPreferences.getInstance();
  final newInput = input.rebuild((it) => it
    ..uname = saved.getString("uname")
    ..token = saved.getString("token"));
  final String body = json.encode(
      serializers.serializeWith(TaskConstructionsRequest.serializer, newInput));
  final resp = await http.post(TASKCONSTRUCTIONS_URL, body: body);
  final result = serializers.deserializeWith(
      TaskConstructionsResponse.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);

  return result;
}
