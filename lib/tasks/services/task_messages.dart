import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/tasks/services/serializers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'task_messages.g.dart';

abstract class TaskMessage implements Built<TaskMessage, TaskMessageBuilder> {
  TaskMessage._();
  factory TaskMessage([updates(TaskMessageBuilder b)]) = _$TaskMessage;

  @BuiltValueField(wireName: 'ID')
  String get id;

  @BuiltValueField(wireName: 'DATETIME_TEXT')
  String get dateTimeText;

  @BuiltValueField(wireName: 'USER')
  String get user;

  @BuiltValueField(wireName: 'USER_TEXT')
  String get userText;

  @BuiltValueField(wireName: "TEXT")
  String get text;

  @BuiltValueField(wireName: 'ATTACHEMENT')
  String get attachement;

  static Serializer<TaskMessage> get serializer => _$taskMessageSerializer;
}

abstract class TaskMessagesRequest
    implements Built<TaskMessagesRequest, TaskMessagesRequestBuilder> {
  TaskMessagesRequest._();
  factory TaskMessagesRequest([updates(TaskMessagesRequestBuilder b)]) =
      _$TaskMessagesRequest;

  @BuiltValueField(wireName: "TOKEN")
  String get token;

  @BuiltValueField(wireName: "UNAME")
  String get uname;

  @BuiltValueField(wireName: "ACTION")
  String get action;

  @BuiltValueField(wireName: "ID")
  String get id;

  @BuiltValueField(wireName: "FILENAME")
  String get fileName;

  @BuiltValueField(wireName: 'MESSAGE')
  String get message;

  static Serializer<TaskMessagesRequest> get serializer =>
      _$taskMessagesRequestSerializer;
}

abstract class TaskMessagesResponse
    implements Built<TaskMessagesResponse, TaskMessagesResponseBuilder> {
  TaskMessagesResponse._();

  @BuiltValueField(wireName: 'ERROR')
  String get error;

  @BuiltValueField(wireName: 'UNAUTHORIZED')
  String get unauthorized;

  @BuiltValueField(wireName: 'MESSAGES')
  BuiltList<TaskMessage> get messages;

  @nullable
  String get currentUser;

  factory TaskMessagesResponse([updates(TaskMessagesResponseBuilder b)]) =
      _$TaskMessagesResponse;

  static Serializer<TaskMessagesResponse> get serializer =>
      _$taskMessagesResponseSerializer;
}

Future<TaskMessagesResponse> callTaskMessages(TaskMessagesRequest input) async {
  if (input.action == 'LOADING') return null;
  final saved = await SharedPreferences.getInstance();
  final newInput = input.rebuild((it) => it
    ..uname = saved.getString("uname")
    ..token = saved.getString("token"));
  final String body = json.encode(
      serializers.serializeWith(TaskMessagesRequest.serializer, newInput));
  final resp = await http.post(TASKMESSAGES_URL, body: body);
  final result = serializers.deserializeWith(
      TaskMessagesResponse.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);

  return result.rebuild((it) => it.currentUser = saved.getString('uname'));
}
