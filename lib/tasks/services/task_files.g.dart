// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_files.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<TaskFilesRequest> _$taskFilesRequestSerializer =
    new _$TaskFilesRequestSerializer();
Serializer<TaskFilesResponse> _$taskFilesResponseSerializer =
    new _$TaskFilesResponseSerializer();

class _$TaskFilesRequestSerializer
    implements StructuredSerializer<TaskFilesRequest> {
  @override
  final Iterable<Type> types = const [TaskFilesRequest, _$TaskFilesRequest];
  @override
  final String wireName = 'TaskFilesRequest';

  @override
  Iterable serialize(Serializers serializers, TaskFilesRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'TOKEN',
      serializers.serialize(object.token,
          specifiedType: const FullType(String)),
      'UNAME',
      serializers.serialize(object.uname,
          specifiedType: const FullType(String)),
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'ATTACHEMENT',
      serializers.serialize(object.attachement,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskFilesRequest deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskFilesRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'TOKEN':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME':
          result.uname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ATTACHEMENT':
          result.attachement = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskFilesResponseSerializer
    implements StructuredSerializer<TaskFilesResponse> {
  @override
  final Iterable<Type> types = const [TaskFilesResponse, _$TaskFilesResponse];
  @override
  final String wireName = 'TaskFilesResponse';

  @override
  Iterable serialize(Serializers serializers, TaskFilesResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
      'UNAUTHORIZED',
      serializers.serialize(object.unauthorized,
          specifiedType: const FullType(String)),
      'FILENAME',
      serializers.serialize(object.fileName,
          specifiedType: const FullType(String)),
      'DATA',
      serializers.serialize(object.data, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskFilesResponse deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskFilesResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAUTHORIZED':
          result.unauthorized = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'FILENAME':
          result.fileName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'DATA':
          result.data = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskFilesRequest extends TaskFilesRequest {
  @override
  final String token;
  @override
  final String uname;
  @override
  final String id;
  @override
  final String attachement;

  factory _$TaskFilesRequest([void updates(TaskFilesRequestBuilder b)]) =>
      (new TaskFilesRequestBuilder()..update(updates)).build();

  _$TaskFilesRequest._({this.token, this.uname, this.id, this.attachement})
      : super._() {
    if (token == null) {
      throw new BuiltValueNullFieldError('TaskFilesRequest', 'token');
    }
    if (uname == null) {
      throw new BuiltValueNullFieldError('TaskFilesRequest', 'uname');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskFilesRequest', 'id');
    }
    if (attachement == null) {
      throw new BuiltValueNullFieldError('TaskFilesRequest', 'attachement');
    }
  }

  @override
  TaskFilesRequest rebuild(void updates(TaskFilesRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskFilesRequestBuilder toBuilder() =>
      new TaskFilesRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskFilesRequest &&
        token == other.token &&
        uname == other.uname &&
        id == other.id &&
        attachement == other.attachement;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, token.hashCode), uname.hashCode), id.hashCode),
        attachement.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskFilesRequest')
          ..add('token', token)
          ..add('uname', uname)
          ..add('id', id)
          ..add('attachement', attachement))
        .toString();
  }
}

class TaskFilesRequestBuilder
    implements Builder<TaskFilesRequest, TaskFilesRequestBuilder> {
  _$TaskFilesRequest _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _uname;
  String get uname => _$this._uname;
  set uname(String uname) => _$this._uname = uname;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _attachement;
  String get attachement => _$this._attachement;
  set attachement(String attachement) => _$this._attachement = attachement;

  TaskFilesRequestBuilder();

  TaskFilesRequestBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _uname = _$v.uname;
      _id = _$v.id;
      _attachement = _$v.attachement;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskFilesRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskFilesRequest;
  }

  @override
  void update(void updates(TaskFilesRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskFilesRequest build() {
    final _$result = _$v ??
        new _$TaskFilesRequest._(
            token: token, uname: uname, id: id, attachement: attachement);
    replace(_$result);
    return _$result;
  }
}

class _$TaskFilesResponse extends TaskFilesResponse {
  @override
  final String error;
  @override
  final String unauthorized;
  @override
  final String fileName;
  @override
  final String data;

  factory _$TaskFilesResponse([void updates(TaskFilesResponseBuilder b)]) =>
      (new TaskFilesResponseBuilder()..update(updates)).build();

  _$TaskFilesResponse._(
      {this.error, this.unauthorized, this.fileName, this.data})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('TaskFilesResponse', 'error');
    }
    if (unauthorized == null) {
      throw new BuiltValueNullFieldError('TaskFilesResponse', 'unauthorized');
    }
    if (fileName == null) {
      throw new BuiltValueNullFieldError('TaskFilesResponse', 'fileName');
    }
    if (data == null) {
      throw new BuiltValueNullFieldError('TaskFilesResponse', 'data');
    }
  }

  @override
  TaskFilesResponse rebuild(void updates(TaskFilesResponseBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskFilesResponseBuilder toBuilder() =>
      new TaskFilesResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskFilesResponse &&
        error == other.error &&
        unauthorized == other.unauthorized &&
        fileName == other.fileName &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, error.hashCode), unauthorized.hashCode),
            fileName.hashCode),
        data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskFilesResponse')
          ..add('error', error)
          ..add('unauthorized', unauthorized)
          ..add('fileName', fileName)
          ..add('data', data))
        .toString();
  }
}

class TaskFilesResponseBuilder
    implements Builder<TaskFilesResponse, TaskFilesResponseBuilder> {
  _$TaskFilesResponse _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  String _unauthorized;
  String get unauthorized => _$this._unauthorized;
  set unauthorized(String unauthorized) => _$this._unauthorized = unauthorized;

  String _fileName;
  String get fileName => _$this._fileName;
  set fileName(String fileName) => _$this._fileName = fileName;

  String _data;
  String get data => _$this._data;
  set data(String data) => _$this._data = data;

  TaskFilesResponseBuilder();

  TaskFilesResponseBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _unauthorized = _$v.unauthorized;
      _fileName = _$v.fileName;
      _data = _$v.data;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskFilesResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskFilesResponse;
  }

  @override
  void update(void updates(TaskFilesResponseBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskFilesResponse build() {
    final _$result = _$v ??
        new _$TaskFilesResponse._(
            error: error,
            unauthorized: unauthorized,
            fileName: fileName,
            data: data);
    replace(_$result);
    return _$result;
  }
}
