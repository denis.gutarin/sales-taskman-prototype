// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_list.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<TaskList> _$taskListSerializer = new _$TaskListSerializer();
Serializer<TaskListRequest> _$taskListRequestSerializer =
    new _$TaskListRequestSerializer();

class _$TaskListSerializer implements StructuredSerializer<TaskList> {
  @override
  final Iterable<Type> types = const [TaskList, _$TaskList];
  @override
  final String wireName = 'TaskList';

  @override
  Iterable serialize(Serializers serializers, TaskList object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
      'UNAUTHORIZED',
      serializers.serialize(object.unauthorized,
          specifiedType: const FullType(String)),
    ];
    if (object.tasks != null) {
      result
        ..add('TASKS')
        ..add(serializers.serialize(object.tasks,
            specifiedType:
                const FullType(BuiltList, const [const FullType(TaskInfo)])));
    }

    return result;
  }

  @override
  TaskList deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskListBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAUTHORIZED':
          result.unauthorized = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'TASKS':
          result.tasks.replace(serializers.deserialize(value,
              specifiedType: const FullType(
                  BuiltList, const [const FullType(TaskInfo)])) as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$TaskListRequestSerializer
    implements StructuredSerializer<TaskListRequest> {
  @override
  final Iterable<Type> types = const [TaskListRequest, _$TaskListRequest];
  @override
  final String wireName = 'TaskListRequest';

  @override
  Iterable serialize(Serializers serializers, TaskListRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'TOKEN',
      serializers.serialize(object.token,
          specifiedType: const FullType(String)),
      'UNAME',
      serializers.serialize(object.uname,
          specifiedType: const FullType(String)),
      'FILTER',
      serializers.serialize(object.filter,
          specifiedType: const FullType(String)),
      'SEARCH',
      serializers.serialize(object.search,
          specifiedType: const FullType(String)),
      'ACTION',
      serializers.serialize(object.action,
          specifiedType: const FullType(String)),
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskListRequest deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskListRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'TOKEN':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME':
          result.uname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'FILTER':
          result.filter = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SEARCH':
          result.search = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ACTION':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskList extends TaskList {
  @override
  final String error;
  @override
  final String unauthorized;
  @override
  final BuiltList<TaskInfo> tasks;

  factory _$TaskList([void updates(TaskListBuilder b)]) =>
      (new TaskListBuilder()..update(updates)).build();

  _$TaskList._({this.error, this.unauthorized, this.tasks}) : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('TaskList', 'error');
    }
    if (unauthorized == null) {
      throw new BuiltValueNullFieldError('TaskList', 'unauthorized');
    }
  }

  @override
  TaskList rebuild(void updates(TaskListBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskListBuilder toBuilder() => new TaskListBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskList &&
        error == other.error &&
        unauthorized == other.unauthorized &&
        tasks == other.tasks;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, error.hashCode), unauthorized.hashCode), tasks.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskList')
          ..add('error', error)
          ..add('unauthorized', unauthorized)
          ..add('tasks', tasks))
        .toString();
  }
}

class TaskListBuilder implements Builder<TaskList, TaskListBuilder> {
  _$TaskList _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  String _unauthorized;
  String get unauthorized => _$this._unauthorized;
  set unauthorized(String unauthorized) => _$this._unauthorized = unauthorized;

  ListBuilder<TaskInfo> _tasks;
  ListBuilder<TaskInfo> get tasks =>
      _$this._tasks ??= new ListBuilder<TaskInfo>();
  set tasks(ListBuilder<TaskInfo> tasks) => _$this._tasks = tasks;

  TaskListBuilder();

  TaskListBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _unauthorized = _$v.unauthorized;
      _tasks = _$v.tasks?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskList other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskList;
  }

  @override
  void update(void updates(TaskListBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskList build() {
    _$TaskList _$result;
    try {
      _$result = _$v ??
          new _$TaskList._(
              error: error, unauthorized: unauthorized, tasks: _tasks?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'tasks';
        _tasks?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TaskList', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$TaskListRequest extends TaskListRequest {
  @override
  final String token;
  @override
  final String uname;
  @override
  final String filter;
  @override
  final String search;
  @override
  final String action;
  @override
  final String id;

  factory _$TaskListRequest([void updates(TaskListRequestBuilder b)]) =>
      (new TaskListRequestBuilder()..update(updates)).build();

  _$TaskListRequest._(
      {this.token, this.uname, this.filter, this.search, this.action, this.id})
      : super._() {
    if (token == null) {
      throw new BuiltValueNullFieldError('TaskListRequest', 'token');
    }
    if (uname == null) {
      throw new BuiltValueNullFieldError('TaskListRequest', 'uname');
    }
    if (filter == null) {
      throw new BuiltValueNullFieldError('TaskListRequest', 'filter');
    }
    if (search == null) {
      throw new BuiltValueNullFieldError('TaskListRequest', 'search');
    }
    if (action == null) {
      throw new BuiltValueNullFieldError('TaskListRequest', 'action');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskListRequest', 'id');
    }
  }

  @override
  TaskListRequest rebuild(void updates(TaskListRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskListRequestBuilder toBuilder() =>
      new TaskListRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskListRequest &&
        token == other.token &&
        uname == other.uname &&
        filter == other.filter &&
        search == other.search &&
        action == other.action &&
        id == other.id;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, token.hashCode), uname.hashCode),
                    filter.hashCode),
                search.hashCode),
            action.hashCode),
        id.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskListRequest')
          ..add('token', token)
          ..add('uname', uname)
          ..add('filter', filter)
          ..add('search', search)
          ..add('action', action)
          ..add('id', id))
        .toString();
  }
}

class TaskListRequestBuilder
    implements Builder<TaskListRequest, TaskListRequestBuilder> {
  _$TaskListRequest _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _uname;
  String get uname => _$this._uname;
  set uname(String uname) => _$this._uname = uname;

  String _filter;
  String get filter => _$this._filter;
  set filter(String filter) => _$this._filter = filter;

  String _search;
  String get search => _$this._search;
  set search(String search) => _$this._search = search;

  String _action;
  String get action => _$this._action;
  set action(String action) => _$this._action = action;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  TaskListRequestBuilder();

  TaskListRequestBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _uname = _$v.uname;
      _filter = _$v.filter;
      _search = _$v.search;
      _action = _$v.action;
      _id = _$v.id;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskListRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskListRequest;
  }

  @override
  void update(void updates(TaskListRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskListRequest build() {
    final _$result = _$v ??
        new _$TaskListRequest._(
            token: token,
            uname: uname,
            filter: filter,
            search: search,
            action: action,
            id: id);
    replace(_$result);
    return _$result;
  }
}
