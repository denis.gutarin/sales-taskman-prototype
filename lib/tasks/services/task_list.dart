import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/tasks/services/serializers.dart';
import 'package:ironman/tasks/services/task_info.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'task_list.g.dart';

abstract class TaskList implements Built<TaskList, TaskListBuilder> {
  TaskList._();
  factory TaskList([updates(TaskListBuilder b)]) = _$TaskList;

  @BuiltValueField(wireName: "ERROR")
  String get error;

  @BuiltValueField(wireName: "UNAUTHORIZED")
  String get unauthorized;

  @BuiltValueField(wireName: "TASKS")
  @nullable
  BuiltList<TaskInfo> get tasks;

  static Serializer<TaskList> get serializer => _$taskListSerializer;
}

abstract class TaskListRequest
    implements Built<TaskListRequest, TaskListRequestBuilder> {
  TaskListRequest._();
  factory TaskListRequest([updates(TaskListRequestBuilder b)]) =
      _$TaskListRequest;

  @BuiltValueField(wireName: "TOKEN")
  String get token;

  @BuiltValueField(wireName: "UNAME")
  String get uname;

  @BuiltValueField(wireName: "FILTER")
  String get filter;

  @BuiltValueField(wireName: "SEARCH")
  String get search;

  @BuiltValueField(wireName: 'ACTION')
  String get action;

  @BuiltValueField(wireName: "ID")
  String get id;

  static Serializer<TaskListRequest> get serializer =>
      _$taskListRequestSerializer;
}

Future<TaskList> callTaskList(TaskListRequest input) async {
  if (input.action == 'LOADING') return null;
  final saved = await SharedPreferences.getInstance();
  final newInput = input.rebuild((it) => it
    ..uname = saved.getString("uname")
    ..token = saved.getString("token"));
  final String body = json
      .encode(serializers.serializeWith(TaskListRequest.serializer, newInput));
  final resp = await http.post(TASKLIST_URL, body: body);
  final result =
      serializers.deserializeWith(TaskList.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);
  return result;
}
