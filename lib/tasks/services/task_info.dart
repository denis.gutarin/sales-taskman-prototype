import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/tasks/services/serializers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'task_info.g.dart';

abstract class TaskInfoRequest
    implements Built<TaskInfoRequest, TaskInfoRequestBuilder> {
  TaskInfoRequest._();
  factory TaskInfoRequest([updates(TaskInfoRequestBuilder b)]) =
      _$TaskInfoRequest;

  @BuiltValueField(wireName: "TOKEN")
  String get token;

  @BuiltValueField(wireName: "UNAME")
  String get uname;

  @BuiltValueField(wireName: "ACTION")
  String get action;

  @BuiltValueField(wireName: "ID")
  String get id;

  static Serializer<TaskInfoRequest> get serializer =>
      _$taskInfoRequestSerializer;
}

abstract class TaskInfoResponse
    implements Built<TaskInfoResponse, TaskInfoResponseBuilder> {
  TaskInfoResponse._();

  @BuiltValueField(wireName: 'ERROR')
  String get error;

  @BuiltValueField(wireName: 'UNAUTHORIZED')
  String get unauthorized;

  @BuiltValueField(wireName: 'TASK')
  TaskInfo get task;

  factory TaskInfoResponse([updates(TaskInfoResponseBuilder b)]) =
      _$TaskInfoResponse;

  static Serializer<TaskInfoResponse> get serializer =>
      _$taskInfoResponseSerializer;
}

abstract class TaskInfo implements Built<TaskInfo, TaskInfoBuilder> {
  TaskInfo._();

  factory TaskInfo([updates(TaskInfoBuilder b)]) = _$TaskInfo;

  @BuiltValueField(wireName: 'ID')
  String get id;

  @BuiltValueField(wireName: 'AUTHOR_ID')
  String get authorId;

  @BuiltValueField(wireName: 'AUTHOR_TEXT')
  String get authorText;

  @BuiltValueField(wireName: 'CREATED_AT')
  DateTime get createdAt;

  @BuiltValueField(wireName: 'EXECUTOR_ID')
  String get executorId;

  @BuiltValueField(wireName: 'EXECUTOR_TEXT')
  String get executorText;

  @BuiltValueField(wireName: 'DEADLINE')
  DateTime get deadline;

  @BuiltValueField(wireName: 'DONE')
  String get done;

  @BuiltValueField(wireName: 'CUSTOMER_ID')
  String get customerId;

  @BuiltValueField(wireName: 'CUSTOMER_TEXT')
  String get customerText;

  @BuiltValueField(wireName: 'CONSTRUCTION_ID')
  String get constructionId;

  @BuiltValueField(wireName: 'CONSTRUCTION_TEXT')
  String get constructionText;

  @BuiltValueField(wireName: 'FIRST_MESSAGE')
  String get firstMessage;

  @BuiltValueField(wireName: 'LAST_MESSAGE')
  String get lastMessage;

  @nullable
  String get currentUser;

  static Serializer<TaskInfo> get serializer => _$taskInfoSerializer;
}

Future<TaskInfoResponse> callTaskInfo(TaskInfoRequest input) async {
  if (input.action == 'LOADING') return null;
  final saved = await SharedPreferences.getInstance();
  final newInput = input.rebuild((it) => it
    ..uname = saved.getString("uname")
    ..token = saved.getString("token"));
  final String body = json
      .encode(serializers.serializeWith(TaskInfoRequest.serializer, newInput));
  final resp = await http.post(TASKINFO_URL, body: body);
  final result = serializers.deserializeWith(
      TaskInfoResponse.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);
  return result
      .rebuild((it) => it..task.currentUser = saved.getString('uname'));
}
