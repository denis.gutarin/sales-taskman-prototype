// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_datetime.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<TaskDatetimeRequest> _$taskDatetimeRequestSerializer =
    new _$TaskDatetimeRequestSerializer();
Serializer<TaskDatetimeResponse> _$taskDatetimeResponseSerializer =
    new _$TaskDatetimeResponseSerializer();

class _$TaskDatetimeRequestSerializer
    implements StructuredSerializer<TaskDatetimeRequest> {
  @override
  final Iterable<Type> types = const [
    TaskDatetimeRequest,
    _$TaskDatetimeRequest
  ];
  @override
  final String wireName = 'TaskDatetimeRequest';

  @override
  Iterable serialize(Serializers serializers, TaskDatetimeRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'TOKEN',
      serializers.serialize(object.token,
          specifiedType: const FullType(String)),
      'UNAME',
      serializers.serialize(object.uname,
          specifiedType: const FullType(String)),
      'ACTION',
      serializers.serialize(object.action,
          specifiedType: const FullType(String)),
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'DATE',
      serializers.serialize(object.date, specifiedType: const FullType(String)),
      'TIME',
      serializers.serialize(object.time, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskDatetimeRequest deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskDatetimeRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'TOKEN':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME':
          result.uname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ACTION':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'DATE':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'TIME':
          result.time = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskDatetimeResponseSerializer
    implements StructuredSerializer<TaskDatetimeResponse> {
  @override
  final Iterable<Type> types = const [
    TaskDatetimeResponse,
    _$TaskDatetimeResponse
  ];
  @override
  final String wireName = 'TaskDatetimeResponse';

  @override
  Iterable serialize(Serializers serializers, TaskDatetimeResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
      'UNAUTHORIZED',
      serializers.serialize(object.unauthorized,
          specifiedType: const FullType(String)),
      'DATE',
      serializers.serialize(object.date, specifiedType: const FullType(String)),
      'TIME',
      serializers.serialize(object.time, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskDatetimeResponse deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskDatetimeResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAUTHORIZED':
          result.unauthorized = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'DATE':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'TIME':
          result.time = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskDatetimeRequest extends TaskDatetimeRequest {
  @override
  final String token;
  @override
  final String uname;
  @override
  final String action;
  @override
  final String id;
  @override
  final String date;
  @override
  final String time;

  factory _$TaskDatetimeRequest([void updates(TaskDatetimeRequestBuilder b)]) =>
      (new TaskDatetimeRequestBuilder()..update(updates)).build();

  _$TaskDatetimeRequest._(
      {this.token, this.uname, this.action, this.id, this.date, this.time})
      : super._() {
    if (token == null) {
      throw new BuiltValueNullFieldError('TaskDatetimeRequest', 'token');
    }
    if (uname == null) {
      throw new BuiltValueNullFieldError('TaskDatetimeRequest', 'uname');
    }
    if (action == null) {
      throw new BuiltValueNullFieldError('TaskDatetimeRequest', 'action');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskDatetimeRequest', 'id');
    }
    if (date == null) {
      throw new BuiltValueNullFieldError('TaskDatetimeRequest', 'date');
    }
    if (time == null) {
      throw new BuiltValueNullFieldError('TaskDatetimeRequest', 'time');
    }
  }

  @override
  TaskDatetimeRequest rebuild(void updates(TaskDatetimeRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskDatetimeRequestBuilder toBuilder() =>
      new TaskDatetimeRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskDatetimeRequest &&
        token == other.token &&
        uname == other.uname &&
        action == other.action &&
        id == other.id &&
        date == other.date &&
        time == other.time;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, token.hashCode), uname.hashCode),
                    action.hashCode),
                id.hashCode),
            date.hashCode),
        time.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskDatetimeRequest')
          ..add('token', token)
          ..add('uname', uname)
          ..add('action', action)
          ..add('id', id)
          ..add('date', date)
          ..add('time', time))
        .toString();
  }
}

class TaskDatetimeRequestBuilder
    implements Builder<TaskDatetimeRequest, TaskDatetimeRequestBuilder> {
  _$TaskDatetimeRequest _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _uname;
  String get uname => _$this._uname;
  set uname(String uname) => _$this._uname = uname;

  String _action;
  String get action => _$this._action;
  set action(String action) => _$this._action = action;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _date;
  String get date => _$this._date;
  set date(String date) => _$this._date = date;

  String _time;
  String get time => _$this._time;
  set time(String time) => _$this._time = time;

  TaskDatetimeRequestBuilder();

  TaskDatetimeRequestBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _uname = _$v.uname;
      _action = _$v.action;
      _id = _$v.id;
      _date = _$v.date;
      _time = _$v.time;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskDatetimeRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskDatetimeRequest;
  }

  @override
  void update(void updates(TaskDatetimeRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskDatetimeRequest build() {
    final _$result = _$v ??
        new _$TaskDatetimeRequest._(
            token: token,
            uname: uname,
            action: action,
            id: id,
            date: date,
            time: time);
    replace(_$result);
    return _$result;
  }
}

class _$TaskDatetimeResponse extends TaskDatetimeResponse {
  @override
  final String error;
  @override
  final String unauthorized;
  @override
  final String date;
  @override
  final String time;

  factory _$TaskDatetimeResponse(
          [void updates(TaskDatetimeResponseBuilder b)]) =>
      (new TaskDatetimeResponseBuilder()..update(updates)).build();

  _$TaskDatetimeResponse._(
      {this.error, this.unauthorized, this.date, this.time})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('TaskDatetimeResponse', 'error');
    }
    if (unauthorized == null) {
      throw new BuiltValueNullFieldError(
          'TaskDatetimeResponse', 'unauthorized');
    }
    if (date == null) {
      throw new BuiltValueNullFieldError('TaskDatetimeResponse', 'date');
    }
    if (time == null) {
      throw new BuiltValueNullFieldError('TaskDatetimeResponse', 'time');
    }
  }

  @override
  TaskDatetimeResponse rebuild(void updates(TaskDatetimeResponseBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskDatetimeResponseBuilder toBuilder() =>
      new TaskDatetimeResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskDatetimeResponse &&
        error == other.error &&
        unauthorized == other.unauthorized &&
        date == other.date &&
        time == other.time;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, error.hashCode), unauthorized.hashCode), date.hashCode),
        time.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskDatetimeResponse')
          ..add('error', error)
          ..add('unauthorized', unauthorized)
          ..add('date', date)
          ..add('time', time))
        .toString();
  }
}

class TaskDatetimeResponseBuilder
    implements Builder<TaskDatetimeResponse, TaskDatetimeResponseBuilder> {
  _$TaskDatetimeResponse _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  String _unauthorized;
  String get unauthorized => _$this._unauthorized;
  set unauthorized(String unauthorized) => _$this._unauthorized = unauthorized;

  String _date;
  String get date => _$this._date;
  set date(String date) => _$this._date = date;

  String _time;
  String get time => _$this._time;
  set time(String time) => _$this._time = time;

  TaskDatetimeResponseBuilder();

  TaskDatetimeResponseBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _unauthorized = _$v.unauthorized;
      _date = _$v.date;
      _time = _$v.time;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskDatetimeResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskDatetimeResponse;
  }

  @override
  void update(void updates(TaskDatetimeResponseBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskDatetimeResponse build() {
    final _$result = _$v ??
        new _$TaskDatetimeResponse._(
            error: error, unauthorized: unauthorized, date: date, time: time);
    replace(_$result);
    return _$result;
  }
}
