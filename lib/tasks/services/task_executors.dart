import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:ironman/tasks/services/serializers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ironman/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'task_executors.g.dart';

abstract class TaskExecutor
    implements Built<TaskExecutor, TaskExecutorBuilder> {
  TaskExecutor._();
  factory TaskExecutor([updates(TaskExecutorBuilder b)]) = _$TaskExecutor;

  @BuiltValueField(wireName: 'UNAME')
  String get id;

  @BuiltValueField(wireName: 'UNAME_TEXT')
  String get text;

  @BuiltValueField(wireName: 'SELECTED')
  String get selected;

  static Serializer<TaskExecutor> get serializer => _$taskExecutorSerializer;
}

abstract class TaskExecutorsRequest
    implements Built<TaskExecutorsRequest, TaskExecutorsRequestBuilder> {
  TaskExecutorsRequest._();
  factory TaskExecutorsRequest([updates(TaskExecutorsRequestBuilder b)]) =
      _$TaskExecutorsRequest;

  @BuiltValueField(wireName: "TOKEN")
  String get token;

  @BuiltValueField(wireName: "UNAME")
  String get uname;

  @BuiltValueField(wireName: "ACTION")
  String get action;

  @BuiltValueField(wireName: "ID")
  String get id;

  @BuiltValueField(wireName: 'EXECUTOR')
  String get executor;

  @BuiltValueField(wireName: 'SEARCH')
  String get search;

  static Serializer<TaskExecutorsRequest> get serializer =>
      _$taskExecutorsRequestSerializer;
}

abstract class TaskExecutorsResponse
    implements Built<TaskExecutorsResponse, TaskExecutorsResponseBuilder> {
  TaskExecutorsResponse._();

  @BuiltValueField(wireName: 'ERROR')
  String get error;

  @BuiltValueField(wireName: 'UNAUTHORIZED')
  String get unauthorized;

  @BuiltValueField(wireName: 'EXECUTORS')
  BuiltList<TaskExecutor> get executors;

  factory TaskExecutorsResponse([updates(TaskExecutorsResponseBuilder b)]) =
      _$TaskExecutorsResponse;

  static Serializer<TaskExecutorsResponse> get serializer =>
      _$taskExecutorsResponseSerializer;
}

Future<TaskExecutorsResponse> callTaskExecutors(
    TaskExecutorsRequest input) async {
  if (input.action == 'LOADING') return null;
  final saved = await SharedPreferences.getInstance();
  final newInput = input.rebuild((it) => it
    ..uname = saved.getString("uname")
    ..token = saved.getString("token"));
  final String body = json.encode(
      serializers.serializeWith(TaskExecutorsRequest.serializer, newInput));
  final resp = await http.post(TASKEXECUTORS_URL, body: body);
  final result = serializers.deserializeWith(
      TaskExecutorsResponse.serializer, json.decode(resp.body));
  if (result.error.isNotEmpty) throw Exception(result.error);

  return result;
}
