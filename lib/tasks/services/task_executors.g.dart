// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_executors.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_catches_without_on_clauses
// ignore_for_file: avoid_returning_this
// ignore_for_file: lines_longer_than_80_chars
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first
// ignore_for_file: unnecessary_const
// ignore_for_file: unnecessary_new
// ignore_for_file: test_types_in_equals

Serializer<TaskExecutor> _$taskExecutorSerializer =
    new _$TaskExecutorSerializer();
Serializer<TaskExecutorsRequest> _$taskExecutorsRequestSerializer =
    new _$TaskExecutorsRequestSerializer();
Serializer<TaskExecutorsResponse> _$taskExecutorsResponseSerializer =
    new _$TaskExecutorsResponseSerializer();

class _$TaskExecutorSerializer implements StructuredSerializer<TaskExecutor> {
  @override
  final Iterable<Type> types = const [TaskExecutor, _$TaskExecutor];
  @override
  final String wireName = 'TaskExecutor';

  @override
  Iterable serialize(Serializers serializers, TaskExecutor object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'UNAME',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'UNAME_TEXT',
      serializers.serialize(object.text, specifiedType: const FullType(String)),
      'SELECTED',
      serializers.serialize(object.selected,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskExecutor deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskExecutorBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'UNAME':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME_TEXT':
          result.text = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SELECTED':
          result.selected = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskExecutorsRequestSerializer
    implements StructuredSerializer<TaskExecutorsRequest> {
  @override
  final Iterable<Type> types = const [
    TaskExecutorsRequest,
    _$TaskExecutorsRequest
  ];
  @override
  final String wireName = 'TaskExecutorsRequest';

  @override
  Iterable serialize(Serializers serializers, TaskExecutorsRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'TOKEN',
      serializers.serialize(object.token,
          specifiedType: const FullType(String)),
      'UNAME',
      serializers.serialize(object.uname,
          specifiedType: const FullType(String)),
      'ACTION',
      serializers.serialize(object.action,
          specifiedType: const FullType(String)),
      'ID',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'EXECUTOR',
      serializers.serialize(object.executor,
          specifiedType: const FullType(String)),
      'SEARCH',
      serializers.serialize(object.search,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  TaskExecutorsRequest deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskExecutorsRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'TOKEN':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAME':
          result.uname = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ACTION':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'ID':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'EXECUTOR':
          result.executor = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'SEARCH':
          result.search = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$TaskExecutorsResponseSerializer
    implements StructuredSerializer<TaskExecutorsResponse> {
  @override
  final Iterable<Type> types = const [
    TaskExecutorsResponse,
    _$TaskExecutorsResponse
  ];
  @override
  final String wireName = 'TaskExecutorsResponse';

  @override
  Iterable serialize(Serializers serializers, TaskExecutorsResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'ERROR',
      serializers.serialize(object.error,
          specifiedType: const FullType(String)),
      'UNAUTHORIZED',
      serializers.serialize(object.unauthorized,
          specifiedType: const FullType(String)),
      'EXECUTORS',
      serializers.serialize(object.executors,
          specifiedType:
              const FullType(BuiltList, const [const FullType(TaskExecutor)])),
    ];

    return result;
  }

  @override
  TaskExecutorsResponse deserialize(
      Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new TaskExecutorsResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'ERROR':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'UNAUTHORIZED':
          result.unauthorized = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'EXECUTORS':
          result.executors.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(TaskExecutor)]))
              as BuiltList);
          break;
      }
    }

    return result.build();
  }
}

class _$TaskExecutor extends TaskExecutor {
  @override
  final String id;
  @override
  final String text;
  @override
  final String selected;

  factory _$TaskExecutor([void updates(TaskExecutorBuilder b)]) =>
      (new TaskExecutorBuilder()..update(updates)).build();

  _$TaskExecutor._({this.id, this.text, this.selected}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskExecutor', 'id');
    }
    if (text == null) {
      throw new BuiltValueNullFieldError('TaskExecutor', 'text');
    }
    if (selected == null) {
      throw new BuiltValueNullFieldError('TaskExecutor', 'selected');
    }
  }

  @override
  TaskExecutor rebuild(void updates(TaskExecutorBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskExecutorBuilder toBuilder() => new TaskExecutorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskExecutor &&
        id == other.id &&
        text == other.text &&
        selected == other.selected;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, id.hashCode), text.hashCode), selected.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskExecutor')
          ..add('id', id)
          ..add('text', text)
          ..add('selected', selected))
        .toString();
  }
}

class TaskExecutorBuilder
    implements Builder<TaskExecutor, TaskExecutorBuilder> {
  _$TaskExecutor _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _text;
  String get text => _$this._text;
  set text(String text) => _$this._text = text;

  String _selected;
  String get selected => _$this._selected;
  set selected(String selected) => _$this._selected = selected;

  TaskExecutorBuilder();

  TaskExecutorBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _text = _$v.text;
      _selected = _$v.selected;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskExecutor other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskExecutor;
  }

  @override
  void update(void updates(TaskExecutorBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskExecutor build() {
    final _$result =
        _$v ?? new _$TaskExecutor._(id: id, text: text, selected: selected);
    replace(_$result);
    return _$result;
  }
}

class _$TaskExecutorsRequest extends TaskExecutorsRequest {
  @override
  final String token;
  @override
  final String uname;
  @override
  final String action;
  @override
  final String id;
  @override
  final String executor;
  @override
  final String search;

  factory _$TaskExecutorsRequest(
          [void updates(TaskExecutorsRequestBuilder b)]) =>
      (new TaskExecutorsRequestBuilder()..update(updates)).build();

  _$TaskExecutorsRequest._(
      {this.token,
      this.uname,
      this.action,
      this.id,
      this.executor,
      this.search})
      : super._() {
    if (token == null) {
      throw new BuiltValueNullFieldError('TaskExecutorsRequest', 'token');
    }
    if (uname == null) {
      throw new BuiltValueNullFieldError('TaskExecutorsRequest', 'uname');
    }
    if (action == null) {
      throw new BuiltValueNullFieldError('TaskExecutorsRequest', 'action');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('TaskExecutorsRequest', 'id');
    }
    if (executor == null) {
      throw new BuiltValueNullFieldError('TaskExecutorsRequest', 'executor');
    }
    if (search == null) {
      throw new BuiltValueNullFieldError('TaskExecutorsRequest', 'search');
    }
  }

  @override
  TaskExecutorsRequest rebuild(void updates(TaskExecutorsRequestBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskExecutorsRequestBuilder toBuilder() =>
      new TaskExecutorsRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskExecutorsRequest &&
        token == other.token &&
        uname == other.uname &&
        action == other.action &&
        id == other.id &&
        executor == other.executor &&
        search == other.search;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, token.hashCode), uname.hashCode),
                    action.hashCode),
                id.hashCode),
            executor.hashCode),
        search.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskExecutorsRequest')
          ..add('token', token)
          ..add('uname', uname)
          ..add('action', action)
          ..add('id', id)
          ..add('executor', executor)
          ..add('search', search))
        .toString();
  }
}

class TaskExecutorsRequestBuilder
    implements Builder<TaskExecutorsRequest, TaskExecutorsRequestBuilder> {
  _$TaskExecutorsRequest _$v;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  String _uname;
  String get uname => _$this._uname;
  set uname(String uname) => _$this._uname = uname;

  String _action;
  String get action => _$this._action;
  set action(String action) => _$this._action = action;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _executor;
  String get executor => _$this._executor;
  set executor(String executor) => _$this._executor = executor;

  String _search;
  String get search => _$this._search;
  set search(String search) => _$this._search = search;

  TaskExecutorsRequestBuilder();

  TaskExecutorsRequestBuilder get _$this {
    if (_$v != null) {
      _token = _$v.token;
      _uname = _$v.uname;
      _action = _$v.action;
      _id = _$v.id;
      _executor = _$v.executor;
      _search = _$v.search;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskExecutorsRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskExecutorsRequest;
  }

  @override
  void update(void updates(TaskExecutorsRequestBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskExecutorsRequest build() {
    final _$result = _$v ??
        new _$TaskExecutorsRequest._(
            token: token,
            uname: uname,
            action: action,
            id: id,
            executor: executor,
            search: search);
    replace(_$result);
    return _$result;
  }
}

class _$TaskExecutorsResponse extends TaskExecutorsResponse {
  @override
  final String error;
  @override
  final String unauthorized;
  @override
  final BuiltList<TaskExecutor> executors;

  factory _$TaskExecutorsResponse(
          [void updates(TaskExecutorsResponseBuilder b)]) =>
      (new TaskExecutorsResponseBuilder()..update(updates)).build();

  _$TaskExecutorsResponse._({this.error, this.unauthorized, this.executors})
      : super._() {
    if (error == null) {
      throw new BuiltValueNullFieldError('TaskExecutorsResponse', 'error');
    }
    if (unauthorized == null) {
      throw new BuiltValueNullFieldError(
          'TaskExecutorsResponse', 'unauthorized');
    }
    if (executors == null) {
      throw new BuiltValueNullFieldError('TaskExecutorsResponse', 'executors');
    }
  }

  @override
  TaskExecutorsResponse rebuild(void updates(TaskExecutorsResponseBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TaskExecutorsResponseBuilder toBuilder() =>
      new TaskExecutorsResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TaskExecutorsResponse &&
        error == other.error &&
        unauthorized == other.unauthorized &&
        executors == other.executors;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, error.hashCode), unauthorized.hashCode),
        executors.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TaskExecutorsResponse')
          ..add('error', error)
          ..add('unauthorized', unauthorized)
          ..add('executors', executors))
        .toString();
  }
}

class TaskExecutorsResponseBuilder
    implements Builder<TaskExecutorsResponse, TaskExecutorsResponseBuilder> {
  _$TaskExecutorsResponse _$v;

  String _error;
  String get error => _$this._error;
  set error(String error) => _$this._error = error;

  String _unauthorized;
  String get unauthorized => _$this._unauthorized;
  set unauthorized(String unauthorized) => _$this._unauthorized = unauthorized;

  ListBuilder<TaskExecutor> _executors;
  ListBuilder<TaskExecutor> get executors =>
      _$this._executors ??= new ListBuilder<TaskExecutor>();
  set executors(ListBuilder<TaskExecutor> executors) =>
      _$this._executors = executors;

  TaskExecutorsResponseBuilder();

  TaskExecutorsResponseBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error;
      _unauthorized = _$v.unauthorized;
      _executors = _$v.executors?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TaskExecutorsResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TaskExecutorsResponse;
  }

  @override
  void update(void updates(TaskExecutorsResponseBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$TaskExecutorsResponse build() {
    _$TaskExecutorsResponse _$result;
    try {
      _$result = _$v ??
          new _$TaskExecutorsResponse._(
              error: error,
              unauthorized: unauthorized,
              executors: executors.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'executors';
        executors.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TaskExecutorsResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}
