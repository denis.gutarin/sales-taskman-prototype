import 'package:flutter/material.dart';
import 'package:ironman/tasks/components/task_list_screen.dart';
import 'package:ironman/tasks/components/task_info_screen.dart';
import 'package:ironman/logon/components/logon_screen.dart';
import 'package:ironman/logon/components/checkcode_screen.dart';
import 'package:ironman/tasks/services/task_list.dart';

void main() {
  final request = TaskListRequest((it) => it
    ..uname = ''
    ..token = ''
    ..filter = 'TODAY'
    ..search = ''
    ..action = ''
    ..id = '');
  callTaskList(request).then((response) {
    runApp(App(TaskListScreen()));
  }, onError: (error) {
    runApp(App(LogonScreen()));
  });
}

class App extends StatelessWidget {
  final Widget firstPage;

  App(this.firstPage);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Айронмен',
      theme: ThemeData(
        primarySwatch: Colors.amber,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => firstPage,
        '/checkcode': (context) => CheckcodeScreen(),
        '/logon': (context) => LogonScreen(),
        '/tasks': (context) => TaskListScreen(),
        '/task_info': (context) => TaskInfoScreen(),
      },
    );
  }
}
